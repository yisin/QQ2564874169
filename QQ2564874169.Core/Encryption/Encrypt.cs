﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace QQ2564874169.Core.Encryption
{
    public static class Encrypt
    {
        public static string SHA256(string value, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            using (var hash = System.Security.Cryptography.SHA256.Create())
            {
                var hashBytes = hash.ComputeHash(encoding.GetBytes(value));
                return Convert.ToBase64String(hashBytes);
            }
        }

        public static string MD5(string data, Encoding encoding = null, bool len16 = false)
        {
            encoding = encoding ?? Encoding.UTF8;
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var hashBytes = md5.ComputeHash(encoding.GetBytes(data));
                if (len16)
                {
                    return BitConverter.ToString(hashBytes, 4, 8).Replace("-", "").ToLower();
                }
                return Convert.ToBase64String(hashBytes);
            }
        }

        public static string SHA1(string data, Encoding encoding = null)
        {
            using (var sha = new SHA1CryptoServiceProvider())
            {
                var enc = encoding ?? Encoding.UTF8;
                var dataToHash = enc.GetBytes(data);
                var hashBytes = sha.ComputeHash(dataToHash);
                return Convert.ToBase64String(hashBytes);
            }
        }

        public static string HmacSHA1(string data, string key, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            using (var hmacsha1 = new HMACSHA1 {Key = encoding.GetBytes(key)})
            {
                var hashBytes = hmacsha1.ComputeHash(encoding.GetBytes(data));
                return Convert.ToBase64String(hashBytes);
            }
        }

        public static string HmacSHA256(string data, string key)
        {
            var keybytes = Encoding.ASCII.GetBytes(key);
            using (var provider = new HMACSHA256(keybytes))
            {
                var databytes = Encoding.ASCII.GetBytes(data);
                var hashBytes = provider.ComputeHash(databytes);
                return Convert.ToBase64String(hashBytes);
            }
        }

        public static string DES(string content, string salt)
        {
            if (string.IsNullOrEmpty(salt))
                throw new ArgumentNullException(nameof(salt));
            if (salt.Length != 8)
                throw new ArgumentException(nameof(salt) + "的长度必须是8位。");
            var data = Encoding.UTF8.GetBytes(content);
            var key = Encoding.ASCII.GetBytes(salt);
            var iv = Encoding.ASCII.GetBytes(salt);
            using (var des = new DESCryptoServiceProvider {Key = key, IV = iv})
            {
                var desEncrypt = des.CreateEncryptor();
                var hashBytes = desEncrypt.TransformFinalBlock(data, 0, data.Length);
                return Convert.ToBase64String(hashBytes);
            }
        }

        public static RSAEncrypt RSA(string content, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            using (var rsa = new RSACryptoServiceProvider())
            {
                var pub = rsa.ToXmlString(false);
                var pri = rsa.ToXmlString(true);
                rsa.FromXmlString(pub);
                var cipherbytes = rsa.Encrypt(encoding.GetBytes(content), false);
                return new RSAEncrypt(cipherbytes, pub, pri);
            }
        }

        public static string TripleDES(string key, string content, Encoding encoding = null)
        {
            if (string.IsNullOrEmpty(content))
                throw new ArgumentNullException(nameof(content));
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));
            encoding = encoding ?? Encoding.UTF8;
            var des = new TripleDESCryptoServiceProvider
            {
                Key = encoding.GetBytes(key),
                Mode = CipherMode.ECB
            };
            var enc = des.CreateEncryptor();
            var data = encoding.GetBytes(content);
            return Convert.ToBase64String(enc.TransformFinalBlock(data, 0, data.Length));
        }

        public class RSAEncrypt
        {
            public byte[] CipherBytes { get; private set; }
            public string PublicKey { get; private set; }
            public string PrivateKey { get; private set; }

            internal RSAEncrypt(byte[] cipherbytes, string publicKey, string privateKey)
            {
                CipherBytes = cipherbytes;
                PublicKey = publicKey;
                PrivateKey = privateKey;
            }

            public string ToBase64()
            {
                return Convert.ToBase64String(CipherBytes);
            }

            public string ToHex()
            {
                return BitConverter.ToString(CipherBytes).Replace("-", "");
            }
        }
    }
}
