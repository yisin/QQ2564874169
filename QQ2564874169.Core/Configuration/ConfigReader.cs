﻿using System;
using System.Configuration;

namespace QQ2564874169.Core.Configuration
{
    public interface IConfigReader : IDisposable
    {
        event Action Changed;

        object Read(string name, Type type = null);
    }

    public class DefaultConfigReader : IConfigReader
    {
        public event Action Changed;

        public object Read(string name, Type type = null)
        {
            return ConfigurationManager.AppSettings[name];
        }

        public void Dispose()
        {
            Changed = null;
        }
    }
}
