﻿using System;

namespace QQ2564874169.Core.Configuration
{
    public static class Configs
    {
        private static IConfigReader _reader;
        public static event Action<IConfigReader> OnSetReader; 

        static Configs()
        {
            SetReader(new DefaultConfigReader());
        }

        public static void SetReader(IConfigReader reader)
        {
            _reader?.Dispose();
            _reader = reader;
            OnSetReader?.Invoke(_reader);
        }

        public static T Get<T>(string name, T defaultValue = default(T))
        {
            var value = _reader.Read(name, typeof(T));
            if (value == null)
                return defaultValue;
            return (T) value;
        }
    }
}
