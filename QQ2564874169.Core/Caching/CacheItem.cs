﻿
namespace QQ2564874169.Core.Caching
{
    public class CacheItem
    {
        public string Key { get; set; }
        public object Value { get; set; }
        /// <summary>
        /// 指定时间间隔到后过期（秒）。
        /// </summary>
        public int? AbsoluteExpiration { get; set; }
        /// <summary>
        /// 闲置时间间隔到期后过期（秒）。
        /// </summary>
        public int? SlidingExpiration { get; set; }
    }
}
