﻿
namespace QQ2564874169.Core.Caching
{
    public interface ICache
    {
        void Save(CacheItem data);
        void Update(string key, object value);
        bool Exists(string key);
        object Get(string key);
        void Remove(string key);
    }
}
