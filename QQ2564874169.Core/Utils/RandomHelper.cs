﻿using System;

namespace QQ2564874169.Core.Utils
{
    public static class RandomHelper
    {
        public static string RandomNumber(int length)
        {
            var key = "";
            do
            {
                key += Guid.NewGuid().GetHashCode().ToString().Replace("-", "");
            } while (key.Length < length);

            return key.Substring(0, length);
        }

        public static string RandomLetter(int length)
        {
            var key = "";
            do
            {
                var code = Math.Abs(Guid.NewGuid().GetHashCode())%26+65;
                key += (char) code;
            } while (key.Length < length);

            return key.Substring(0, length);
        }

        public static string RandomText(int length)
        {
            var key = "";
            do
            {
                key += Guid.NewGuid().ToString().Replace("-", "").ToLower();
            } while (key.Length < length);

            return key.Substring(0, length);
        }
    }
}
