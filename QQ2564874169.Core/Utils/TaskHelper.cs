﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace QQ2564874169.Core.Utils
{
    public static class TaskHelper
    {
        public static Task Delay(int millisecondes)
        {
            return new Task(() => Thread.Sleep(millisecondes));
        }

        public static Task Run(Action action)
        {
            return Task.Factory.StartNew(action);
        }

        public static Task Run(Action<object> action, object state = null)
        {
            return Task.Factory.StartNew(action, state);
        }
    }
}
