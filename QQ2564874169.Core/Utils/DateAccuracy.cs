﻿
namespace QQ2564874169.Core.Utils
{
    public enum DateAccuracy
    {
        Year = 1,
        Month = 2,
        Day = 3,
        Hour = 4,
        Minute = 5,
        Second = 6,
        Millisecond = 7
    }
}
