﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace QQ2564874169.Core.Utils
{
    public class HttpHelper
    {
        static HttpHelper()
        {
            ServicePointManager.ServerCertificateValidationCallback += (p1, p2, p3, p4) => true;
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;
            ServicePointManager.CheckCertificateRevocationList = false;
        }

        public static HttpWebRequest Request(
            string url, 
            string method, 
            string body = null,
            IDictionary<string, string> header = null,
            Encoding encoding = null)
        {
            var req = WebRequest.Create(url) as HttpWebRequest;
            if (req == null) return null;
            header = header ?? new Dictionary<string, string>();
            encoding = encoding ?? Encoding.UTF8;
            req.Method = method;
            req.KeepAlive = false;
            req.AllowAutoRedirect = false;
            req.Accept = header.GetOrDefault("Accept", "text/html,application/xhtml+xml,application/xml");
            req.ContentType = header.GetOrDefault("ContentType");
            req.Host = header.GetOrDefault("Host", new Uri(url).Host);
            req.Referer = header.GetOrDefault("Referer");
            req.UserAgent = header.GetOrDefault("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36");
            foreach (var key in header.Keys)
            {
                switch (key.ToLower())
                {
                    case "accept":
                    case "contenttype":
                    case "host":
                    case "referer":
                    case "user-agent":
                        break;
                    default:
                        req.Headers.Add(key, header[key]);
                        break;
                }
            }
            if (!string.IsNullOrEmpty(body))
            {
                var data = encoding.GetBytes(body);
                req.ContentLength = data.Length;
                if (string.IsNullOrEmpty(req.ContentType))
                {
                    req.ContentType = "application/x-www-form-urlencoded; charset=" + encoding.BodyName.ToUpper();
                }
                using (var sm = req.GetRequestStream())
                {
                    sm.Write(data, 0, data.Length);
                }
            }
            return req;
        }

        public static WebResponse Get(string url, Action<WebRequest> before = null)
        {
            var req = Request(url, "GET");
            before?.Invoke(req);
            return req.GetResponse();
        }

        public static WebResponse Post(string url, string body = null, Encoding encoding = null,
            Action<WebRequest> before = null)
        {
            var req = Request(url, "POST", body, encoding: encoding);
            before?.Invoke(req);
            return req.GetResponse();
        }

        public static WebResponse Delete(string url, Action<WebRequest> before = null)
        {
            var req = Request(url, "DELETE");
            before?.Invoke(req);
            return req.GetResponse();
        }

        public static WebResponse Put(string url, Action<WebRequest> before = null)
        {
            var req = Request(url, "PUT");
            before?.Invoke(req);
            return req.GetResponse();
        }
    }
}
