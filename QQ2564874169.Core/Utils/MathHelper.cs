﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QQ2564874169.Core.Utils
{
    public static class MathHelper
    {
        public static decimal Round(decimal num, int dec = 2)
        {
            return Math.Round(num, dec, MidpointRounding.AwayFromZero);
        }
    }
}
