﻿using System;

namespace QQ2564874169.Core
{
    public interface ITypeContainer : IDisposable
    {
        ITypeContainer NewContainer(params ServiceParam[] param);

        object Resolve(Type serviceType);
    }

    public class ServiceParam
    {
        public Type ImplementationType { get; set; }
        public Type ServiceType { get; set; }
        public ServiceScope Scope { get; set; }
        public object Instance { get; set; }
        public bool IsExternallyOwned { get; set; }
    }

    public enum ServiceScope
    {
        Dependency,
        Container,
        Single
    }
}
