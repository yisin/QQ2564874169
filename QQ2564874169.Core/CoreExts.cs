﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using QQ2564874169.Core.Caching;

namespace QQ2564874169.Core
{
    public static class CoreExts
    {
        public static bool QQEquals(this string str, string value)
        {
            return string.Equals(str, value, StringComparison.OrdinalIgnoreCase);
        }

        public static string[] QQSplit(this string str, char separator)
        {
            return str.Split(new[] {separator}, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string QQJoin(this string[] array, string separator)
        {
            return string.Join(separator, array);
        }

        public static bool QQStartsWith(this string str, string value)
        {
            return str.StartsWith(value, StringComparison.OrdinalIgnoreCase);
        }

        public static bool QQEndsWith(this string str, string value)
        {
            return str.EndsWith(value, StringComparison.OrdinalIgnoreCase);
        }

        public static TV GetOrDefault<TK, TV>(this IDictionary<TK, TV> dict, TK key, TV defaultValue = default(TV))
        {
            return dict.ContainsKey(key) ? dict[key] : defaultValue;
        }

        public static string Read(this WebResponse response, Encoding encoding = null, bool isGzip = false)
        {
            if (response == null) return null;
            encoding = encoding ?? Encoding.UTF8;
            using (var sm = response.GetResponseStream())
            {
                if (sm == null)
                    throw new NullReferenceException("ResponseStream");
                var stream = sm;
                if (isGzip || "gzip".QQEquals(response.Headers.Get("Content-Encoding")))
                {
                    stream = new GZipStream(sm, CompressionMode.Decompress);
                }
                using (stream)
                {
                    using (var reader = new StreamReader(stream, encoding))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        public static object GetDefaultValue(this Type t)
        {
            return t.IsValueType ? Activator.CreateInstance(t) : null;
        }

        public static T Get<T>(this ICache cache, string key)
        {
            try
            {
                return (T) cache.Get(key);
            }
            catch
            {
                return default(T);
            }
        }

        public static void Save(this ICache cache, string key, object value, int timeout)
        {
            cache.Save(new CacheItem
            {
                Key = key,
                Value = value,
                AbsoluteExpiration = timeout
            });
        }

        public static T Resolve<T>(this ITypeContainer container)
        {
            var obj = container.Resolve(typeof(T));
            if (obj != null)
                return (T) obj;
            return default(T);
        }
    }
}
