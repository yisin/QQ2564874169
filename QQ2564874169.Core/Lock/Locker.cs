﻿
namespace QQ2564874169.Core.Lock
{
    public interface ILocker
    {
        bool IsLock(string key);
        bool Lock(string key);
        void UnLock(string key);
    }
}
