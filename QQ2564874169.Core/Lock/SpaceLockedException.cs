﻿using System;

namespace QQ2564874169.Core.Lock
{
    public class SpaceLockedException : Exception
    {
        public SpaceLockedException()
        {
            
        }

        public SpaceLockedException(string message) : base(message)
        {
            
        }

        public SpaceLockedException(string message, Exception ex) : base(message, ex)
        {
            
        }
    }
}
