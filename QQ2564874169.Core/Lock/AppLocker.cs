﻿using System.Collections.Concurrent;

namespace QQ2564874169.Core.Lock
{
    public class AppLocker : ILocker
    {
        private ConcurrentDictionary<string, bool> _map = new ConcurrentDictionary<string, bool>();

        public bool IsLock(string key)
        {
            return _map.ContainsKey(key);
        }

        public bool Lock(string key)
        {
            return _map.TryAdd(key, true);
        }

        public void UnLock(string key)
        {
            bool r;
            _map.TryRemove(key, out r);
        }
    }
}
