﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QQ2564874169.Core.Utils;

namespace 新商盟
{
    internal static class Exts
    {
        public static DateTime ToDate(this long time, DateAccuracy accuracy = DateAccuracy.Second)
        {
            return ConvertHelper.ToDate(time, accuracy);
        }

        public static long ToLongTime(this DateTime time, DateAccuracy accuracy = DateAccuracy.Second)
        {
            return ConvertHelper.ToLongTime(time, accuracy);
        }
    }
}
