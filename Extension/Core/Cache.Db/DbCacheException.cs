﻿using System;

namespace QQ2564874169.Cache.Db
{
    public class DbCacheException : Exception
    {
        public DbCacheException(string message) : base(message)
        {
            
        }
    }
}
