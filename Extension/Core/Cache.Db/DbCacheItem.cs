﻿using System;

namespace QQ2564874169.Cache.Db
{
    public class DbCacheItem
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string ValueType { get; set; }
        public int? AbsoluteExpiration { get; set; }
        public int? SlidingExpiration { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? GetTime { get; set; }
    }
}
