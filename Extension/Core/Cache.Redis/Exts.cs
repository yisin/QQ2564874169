﻿using System;
using System.Linq;

namespace QQ2564874169.Cache.Redis
{
    internal static class Exts
    {
        public static TimeSpan? GetExpiry(this InternalCacheItem item, int? defaultExpiration)
        {
            TimeSpan? timeSpan = null;
            if (item.AbsoluteExpiration.HasValue)
            {
                var now = DateTime.Now;
                timeSpan = now.AddSeconds(item.AbsoluteExpiration.Value) - now;
            }
            else if (item.SlidingExpiration.HasValue)
            {
                timeSpan = TimeSpan.FromSeconds(item.SlidingExpiration.Value);
            }
            else if (defaultExpiration.HasValue)
            {
                timeSpan = TimeSpan.FromSeconds(defaultExpiration.Value);
            }
            return timeSpan;
        }

        public static string GetAssemblyQualifiedName(this Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return type.GetGenericArguments().First().AssemblyQualifiedName;
            }
            return type.AssemblyQualifiedName;
        }
    }
}
