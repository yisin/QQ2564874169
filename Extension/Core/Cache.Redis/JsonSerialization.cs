﻿using System;
using QQ2564874169.Json;

namespace QQ2564874169.Cache.Redis
{
    internal class JsonSerialization : RedisCache.ISerialization
    {
        public object Deserialize(Type type, string data)
        {
            return JsonHelper.JsonTo(data, type);
        }

        public string Serialize(object obj)
        {
            return JsonHelper.ToJson(obj);
        }
    }
}
