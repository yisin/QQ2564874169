﻿
namespace QQ2564874169.Cache.Redis
{
    internal class InternalCacheItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string ValueType { get; set; }
        public int? AbsoluteExpiration { get; set; }
        public int? SlidingExpiration { get; set; }
    }
}
