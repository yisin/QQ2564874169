﻿using System;

namespace QQ2564874169.RelationalSql.Trigger
{
    public class TriggerBeforeArgs : EventArgs
    {
        public DbExecuteOperation Operation { get; internal set; }
        public object Model { get; internal set; }
        public Type TableType { get; internal set; }
        public bool Cancel { get; set; }
        public string State { get; set; }
    }
}
