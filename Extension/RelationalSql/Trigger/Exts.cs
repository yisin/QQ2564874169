﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QQ2564874169.RelationalSql.Trigger
{
    internal static class Exts
    {
        private static DateTime _startTime = new DateTime(1970, 1, 1).ToUniversalTime();

        public static long ToLong(this DateTime time)
        {
            return (long) (time.ToUniversalTime() - _startTime).TotalMilliseconds;
        }

        public static DateTime ToDateTime(this long time)
        {
            return _startTime.AddMilliseconds(time);
        }

        public static TriggerType ToTriggerType(this DbExecuteOperation operation)
        {
            switch (operation)
            {
                case DbExecuteOperation.Delete:
                    return TriggerType.Delete;
                case DbExecuteOperation.Insert:
                    return TriggerType.Insert;
                case DbExecuteOperation.Update:
                    return TriggerType.Update;
                case DbExecuteOperation.SetNull:
                    return TriggerType.SetNull;
                case DbExecuteOperation.Proc:
                    return TriggerType.Proc;
                default:
                    throw new NotSupportedException(operation.ToString());
            }
        }

        public static DbExecuteOperation ToDbExecuteOperation(this TriggerType type)
        {
            switch (type)
            {
                case TriggerType.Insert:
                    return DbExecuteOperation.Insert;
                case TriggerType.Delete:
                    return DbExecuteOperation.Delete;
                case TriggerType.Update:
                    return DbExecuteOperation.Update;
                case TriggerType.SetNull:
                    return DbExecuteOperation.SetNull;
                case TriggerType.Proc:
                    return DbExecuteOperation.Proc;
                default:
                    throw new NotSupportedException(type.ToString());
            }
        }
    }
}
