﻿using System;

namespace QQ2564874169.RelationalSql.Trigger
{
    public class TriggerReceiveEventArgs : EventArgs
    {
        public TriggerItem Item { get; internal set; }
        public bool Reconsume { get; set; }
    }

    public class TriggerReceiveErrorEventArgs : EventArgs
    {
        public Exception Error { get; internal set; }
        public TriggerItem Item { get; internal set; }
    }

    public class TriggerSubscriberErrorEventArgs : EventArgs
    {
        public Exception Error { get; internal set; }
        public bool ExitTask { get; set; }
    }

    public class TriggerWatchErrorEventArgs : EventArgs
    {
        public Exception Error { get; internal set; }
        public object Service { get; internal set; }
        public Type WatchType { get; internal set; }
        public TriggerType Type { get; internal set; }
        public object Model { get; internal set; }
    }
}
