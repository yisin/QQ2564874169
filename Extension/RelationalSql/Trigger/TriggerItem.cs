﻿
using System;

namespace QQ2564874169.RelationalSql.Trigger
{
    public enum TriggerType
    {
        Insert = 1,
        Delete = 2,
        Update = 3,
        SetNull = 4,
        Proc = 5
    }

    public class TriggerItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public TriggerType? Type { get; set; }
        public string TableType { get; set; }
        public string EntityType { get; set; }
        public string Entity { get; set; }
        public long? CreateTime { get; set; }
        public bool? Processed { get; set; }
        public long? ProcessTime { get; set; }
        public string State { get; set; }

        public TriggerItem()
        {
            if(_initSchema == false)
                throw new ArgumentException($"尚未初始化数据库表结构，请先调用{nameof(TriggerItem)}.{nameof(CreateSchema)}。");
        }
        private static bool _initSchema;

        public static void CreateSchema(ISchemaCreater creater)
        {
            if(_initSchema)
                return;
            if (creater == null)
                throw new ArgumentNullException(nameof(creater));

            using (creater)
            {
                var t = typeof(TriggerItem);
                creater.CreateTable(t);
                creater.CreateIndex(new[] {t.GetProperty(nameof(Id)), t.GetProperty(nameof(CreateTime))});
            }
            _initSchema = true;
        }
    }
}
