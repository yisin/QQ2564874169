﻿
namespace QQ2564874169.RelationalSql.Trigger
{
    public class UpdateArgs
    {
        public object Where { get; set; }
        public object Sets { get; set; }
        public bool IsSetNull { get; set; }
        public string TableType { get; set; }
    }
}
