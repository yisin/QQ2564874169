﻿using System;
using System.Net;
using System.Text;

namespace QQ2564874169.Aliyun.ShortMessage
{
    public abstract class SysParam
    {
        public string AccessKeyId { get; set; }
        public string AccessSecret { get; set; }
        public string Timestamp { get; set; }
        public string Format { get; set; }
        public string SignatureMethod { get; set; }
        public string SignatureVersion { get; set; }
        public string SignatureNonce { get; set; }

        protected SysParam()
        {
            Format = "JSON";
            SignatureMethod = "HMAC-SHA1";
            SignatureVersion = "1.0";
            SignatureNonce = Guid.NewGuid().ToString().ToLower();
            Timestamp = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
        }

        public static string UrlEncode(string url)
        {
            var data = Encoding.UTF8.GetBytes(url);
            data = WebUtility.UrlEncodeToBytes(data, 0, data.Length);
            return Encoding.UTF8.GetString(data).Replace("+", "%20").Replace("*", "%2A").Replace("%7E", "~");
        }
    }
}
