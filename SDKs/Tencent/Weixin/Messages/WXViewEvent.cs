﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QQ2564874169.Tencent.Weixin.Messages
{
    public class WXViewEvent : WXEventMsg
    {
        public string EventKey { get; set; }

        public WXViewEvent(string outerXml) : base(outerXml)
        {
        }
    }
}
