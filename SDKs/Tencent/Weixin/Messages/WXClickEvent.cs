﻿namespace QQ2564874169.Tencent.Weixin.Messages
{
    public class WXClickEvent : WXEventMsg
    {
        public string EventKey { get; set; }

        public WXClickEvent(string outerXml) : base(outerXml)
        {
        }
    }
}