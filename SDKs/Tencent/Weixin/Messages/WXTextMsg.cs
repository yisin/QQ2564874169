﻿
namespace QQ2564874169.Tencent.Weixin.Messages
{
    public class WXTextMsg : WXNormalMsg
    {
        public string Content { get; set; }

        public WXTextMsg(string outerXml) : base(outerXml)
        {
        }
    }
}