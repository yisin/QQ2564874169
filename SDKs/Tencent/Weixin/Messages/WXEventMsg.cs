﻿
namespace QQ2564874169.Tencent.Weixin.Messages
{
    public enum WXEventType
    {
        Click,
        View
    }

    public abstract class WXEventMsg : WXMessage
    {
        public string ToUserName { get; set; }
        public WXEventType Event { get; set; }

        protected WXEventMsg(string outerXml) : base(outerXml)
        {
        }
    }
}