﻿using QQ2564874169.Tencent.Weixin.Response;

namespace QQ2564874169.Tencent.Weixin.Notifys
{
    public class WXNotifyReturn : WXEntity
    {
        public RequestReturnCode return_code { get; set; }
        public string return_msg { get; set; }

        public WXNotifyReturn(RequestReturnCode code = RequestReturnCode.SUCCESS)
        {
            return_code = code;
        }
    }
}
