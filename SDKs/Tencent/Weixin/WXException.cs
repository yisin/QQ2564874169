﻿using System;
using QQ2564874169.Tencent.Weixin.Models;

namespace QQ2564874169.Tencent.Weixin
{
    public class WXException : Exception
    {
        public WXException(string message = null) : base(message)
        {
            
        }

        public WXException(WXReturn wxmsg) : this($"[{wxmsg.errcode}]:{wxmsg.errmsg}")
        {

        }
    }
}
