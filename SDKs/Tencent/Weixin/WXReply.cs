﻿using System;

namespace QQ2564874169.Tencent.Weixin
{
    public enum WXRepyMsgType
    {
        text
    }

    public abstract class WXReply : WXEntity
    {
        public string ToUserName { get; set; }
        public string FromUserName { get; set; }
        public DateTime CreateTime { get; }
        public WXRepyMsgType MsgType { get; }

        protected WXReply(WXRepyMsgType msgType)
        {
            MsgType = msgType;
            CreateTime = DateTime.Now;
        }
    }
}