﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QQ2564874169.Tencent.Weixin.Replys
{
    public class WXReplyText : WXReply
    {
        public string Content { get; set; }

        public WXReplyText() : base(WXRepyMsgType.text)
        {
        }
    }
}
