﻿
namespace QQ2564874169.Tencent.Weixin.Models
{
    public class WXUser
    {
        public string OpenId { get; set; }
        public string Nickname { get; set; }
        public string Sex { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string HeadImgurl { get; set; }
        public string[] Privilege { get; set; }
        public string Unionid { get; set; }
    }
}