﻿
namespace QQ2564874169.Tencent.Weixin.Models
{
    public class WXReturn
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}