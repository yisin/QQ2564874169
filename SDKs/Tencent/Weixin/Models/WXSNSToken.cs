﻿
namespace QQ2564874169.Tencent.Weixin.Models
{
    public class WXSnsToken
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string openid { get; set; }
        public string scope { get; set; }
    }
}
