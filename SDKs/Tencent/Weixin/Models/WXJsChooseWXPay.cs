﻿
namespace QQ2564874169.Tencent.Weixin.Models
{
    public class WXJsChooseWXPay
    {
        public string AppId { get; set; }
        public long TimeStamp { get; set; }
        public string NonceStr { get; set; }
        public string Package { get; set; }
        public string SignType { get; set; }
        public string PaySign { get; set; }
    }
}
