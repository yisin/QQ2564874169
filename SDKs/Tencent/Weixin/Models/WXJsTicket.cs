﻿
namespace QQ2564874169.Tencent.Weixin.Models
{
    public class WXJsTicket
    {
        public string ticket { get; set; }
        public int expires_in { get; set; }
        public long create_time { get; set; }
    }
}