﻿using System.Collections.Generic;

namespace QQ2564874169.Tencent.Weixin.Models
{
    public abstract class WXButton
    {
        public string name { get; set; }
    }

    public class WXButtonList : WXButton
    {
        public List<WXButton> sub_button { get; }

        public WXButtonList(params WXButton[] btns)
        {
            sub_button = new List<WXButton>();
            if (btns != null && btns.Length > 0)
            {
                sub_button.AddRange(btns);
            }
        }
    }

    public class WXViewButton : WXButton
    {
        public string type { get; }
        public string url { get; set; }

        public WXViewButton()
        {
            type = "view";
        }
    }

    public class WXClickButton : WXButton
    {
        public string type { get; }
        public string key { get; set; }

        public WXClickButton()
        {
            type = "click";
        }
    }
}