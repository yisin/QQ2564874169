﻿
namespace QQ2564874169.Tencent.Weixin.Models
{
    public class WXAccessToken
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public long create_time { get; set; }
    }
}