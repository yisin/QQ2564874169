﻿
namespace QQ2564874169.Tencent.Weixin.Models
{
    public class WXJsSign
    {
        public string appId { get; set; }
        public string nonceStr { get; set; }
        public long timestamp { get; set; }
        public string signature { get; set; }
    }
}