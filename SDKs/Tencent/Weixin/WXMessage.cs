﻿using System;

namespace QQ2564874169.Tencent.Weixin
{
    public enum WXMsgType
    {
        Event,
        Text
    }

    public abstract class WXMessage : WXXml
    {
        public string FromUserName { get; set; }
        public DateTime CreateTime { get; set; }
        public WXMsgType MsgType { get; set; }

        protected WXMessage(string outerXml) : base(outerXml)
        {
        }
    }

    public abstract class WXNormalMsg : WXMessage
    {
        public string ToUserName { get; set; }
        public string MsgId { get; set; }

        protected WXNormalMsg(string outerXml) : base(outerXml)
        {
        }
    }
}