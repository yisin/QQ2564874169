﻿using System;
using System.Collections.Generic;
using System.Linq;
using QQ2564874169.Core.Encryption;

namespace QQ2564874169.Tencent.Weixin
{
    public enum SignType
    {
        MD5,
        HMAC_SHA256
    }

    public static class WxSignHelper
    {
        public static string PaySign(IDictionary<string, string> dictionary, string signkey, SignType signType)
        {
            var dict = new SortedDictionary<string, string>(StringComparer.Ordinal);
            foreach (var name in dictionary.Keys.ToArray())
            {
                dict.Add(name, dictionary[name]);
            }
            var tempstr = string.Join("&", dict.Keys.Select(k => k + "=" + dict[k]))+ "&key=" + signkey;
            string sign;
            if (signType == SignType.MD5)
            {
                sign = Encrypt.MD5(tempstr);
            }
            else if (signType == SignType.HMAC_SHA256)
            {
                sign = Encrypt.HmacSHA256(tempstr, signkey);
            }
            else
            {
                throw new ArgumentException("不支持的签名算法：" + signType);
            }
            return sign.Base64ToHex();
        }
    }
}
