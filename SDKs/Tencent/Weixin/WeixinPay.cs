﻿using QQ2564874169.Core;
using QQ2564874169.Core.Utils;
using QQ2564874169.Tencent.Weixin.Request;
using QQ2564874169.Tencent.Weixin.Response;

namespace QQ2564874169.Tencent.Weixin
{
    public class WeixinPay
    {
        public static UnifiedorderResponse Post(Unifiedorder model, string pay_secret)
        {
            var xml = model.ToXml(pay_secret);
            var data = HttpHelper.Post("https://api.mch.weixin.qq.com/pay/unifiedorder", xml.OuterXml).Read();
            var resp = new UnifiedorderResponse(model, data);
            if (resp.CheckSign(pay_secret) == false)
                throw new WXException("返回内容验签失败。");
            return resp;
        }
    }
}
