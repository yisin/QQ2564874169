﻿namespace QQ2564874169.Tencent.Weixin.Request
{
    public class UnifiedorderSceneInfo
    {
        /// <summary>
        /// 门店唯一标识
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 门店名称 
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 门店所在地行政区划码
        /// </summary>
        public string area_code { get; set; }
        /// <summary>
        /// 门店详细地址 
        /// </summary>
        public string address { get; set; }
    }
}
