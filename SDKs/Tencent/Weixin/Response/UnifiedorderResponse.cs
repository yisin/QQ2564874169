﻿using QQ2564874169.Core;
using QQ2564874169.Tencent.Weixin.Request;

namespace QQ2564874169.Tencent.Weixin.Response
{
    public class UnifiedorderResponse : WXXml
    {
        public RequestReturnCode return_code { get; set; }
        public string return_msg { get; set; }
        public string appid { get; set; }
        public string mch_id { get; set; }
        public string device_info { get; set; }
        public string nonce_str { get; set; }
        public string sign { get; set; }
        public BusinessReturnCode result_code { get; set; }
        public UnifiedorderErrCode err_code { get; set; }
        public string err_code_des { get; set; }
        public TradeType trade_type { get; set; }
        public string prepay_id { get; set; }
        public string code_url { get; set; }
        public Unifiedorder Request { get; }

        public UnifiedorderResponse(Unifiedorder request, string outerXml) : base(outerXml)
        {
            Request = request;
        }

        public bool CheckSign(string pay_secret)
        {
            var dict = Xml.ToDictionary(node => !node.Name.QQEquals(nameof(sign)));
            var xmlsign = WxSignHelper.PaySign(dict, pay_secret, Request.sign_type);
            return xmlsign.QQEquals(sign);
        }
    }

    public enum UnifiedorderErrCode
    {
        /// <summary>
        /// 商户未开通此接口权限
        /// </summary>
        NOAUTH,
        /// <summary>
        /// 用户帐号余额不足
        /// </summary>
        NOTENOUGH,
        /// <summary>
        /// 商户订单已支付，无需重复操作
        /// </summary>
        ORDERPAID,
        /// <summary>
        /// 当前订单已关闭，无法支付
        /// </summary>
        ORDERCLOSED,
        /// <summary>
        /// 系统超时
        /// </summary>
        SYSTEMERROR,
        /// <summary>
        /// 参数中缺少APPID
        /// </summary>
        APPID_NOT_EXIST,
        /// <summary>
        /// 参数中缺少MCHID
        /// </summary>
        MCHID_NOT_EXIST,
        /// <summary>
        /// appid和mch_id不匹配
        /// </summary>
        APPID_MCHID_NOT_MATCH,
        /// <summary>
        /// 缺少必要的请求参数
        /// </summary>
        LACK_PARAMS,
        /// <summary>
        /// 同一笔交易不能多次提交
        /// </summary>
        OUT_TRADE_NO_USED,
        /// <summary>
        /// 参数签名结果不正确
        /// </summary>
        SIGNERROR,
        /// <summary>
        /// XML格式错误
        /// </summary>
        XML_FORMAT_ERROR,
        /// <summary>
        /// 未使用post传递参数 
        /// </summary>
        REQUIRE_POST_METHOD,
        /// <summary>
        /// post数据不能为空
        /// </summary>
        POST_DATA_EMPTY,
        /// <summary>
        /// 未使用指定编码格式
        /// </summary>
        NOT_UTF8
    }
}
