﻿using System;
using System.Reflection;
using System.Xml;

namespace QQ2564874169.Tencent.Weixin
{
    public abstract class WXXml
    {
        protected XmlDocument Xml { get; }

        protected WXXml(string outerXml)
        {
            Xml = new XmlDocument {XmlResolver = null};
            Xml.LoadXml(outerXml);
            LoadFromXml();
        }

        private void LoadFromXml()
        {
            var root = Xml.DocumentElement;

            if (root == null)
                throw new Exception("没有根节点。");

            foreach (var node in root.ToArray())
            {
                if (!string.IsNullOrEmpty(node.InnerXml))
                {
                    SetValue(node);
                }
            }
        }

        protected virtual void SetValue(XmlNode xmlNode)
        {
            var p = GetType().GetProperty(xmlNode.Name, 
                BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);

            if (p == null)
                return;

            var value = GetValue(p.PropertyType, xmlNode);

            if (value != null && p.CanWrite)
            {
                p.SetValue(this, value, null);
            }
        }

        protected virtual object GetValue(Type type, XmlNode node)
        {
            if (string.IsNullOrEmpty(node.InnerXml))
                return null;

            var pt = type;

            if (pt == typeof(string) && !string.IsNullOrEmpty(node.InnerText))
            {
                return node.InnerText;
            }
            if (pt == typeof(int) && !string.IsNullOrEmpty(node.InnerText))
            {
                return Convert.ToInt32(node.InnerText);
            }
            if (pt == typeof(long) && !string.IsNullOrEmpty(node.InnerText))
            {
                return Convert.ToInt64(node.InnerText);
            }
            if (pt == typeof(decimal) && !string.IsNullOrEmpty(node.InnerText))
            {
                return Convert.ToDecimal(node.InnerText);
            }
            if (pt == typeof(bool) && !string.IsNullOrEmpty(node.InnerText))
            {
                var b = node.InnerText.ToLower().Trim();
                return b == "0" || b == "true";
            }
            if (pt == typeof(DateTime) && !string.IsNullOrEmpty(node.InnerText))
            {
                var l = Convert.ToInt64(node.InnerText);
                return l.ToDate();
            }
            if (pt.IsEnum && !string.IsNullOrEmpty(node.InnerText))
            {
                try
                {
                    return Enum.Parse(pt, node.InnerText, true);
                }
                catch
                {
                    int num;
                    if (int.TryParse(node.InnerText, out num) == false && node.InnerText.Length == 1)
                    {
                        num = node.InnerText[0];
                    }
                    return Enum.Parse(pt, num.ToString());
                }
            }
            return null;
        }
    }
}