﻿using System;

namespace QQ2564874169.RelationalSql
{
    public class SqlContext
    {
        public string Command { get; internal set; }
        public object Param { get; internal set; }
        public bool IsProc { get; internal set; }
        public int? Timeout { get; internal set; }

        internal SqlContext() { }
    }

    public class SqlException : EventArgs
    {
        public SqlContext Context { get; internal set; }
        public Exception Error { get; internal set; }

        internal SqlException() { }
    }

    public class SqlLapseEventArgs : EventArgs
    {
        public SqlException Error { get; internal set; }

        internal SqlLapseEventArgs() { }
    }

    public class SqlBeforeEventArgs : EventArgs
    {
        public SqlContext Context { get; internal set; }
        public object Result { get; set; }

        internal SqlBeforeEventArgs() { }
    }

    public class SqlAfterEventArgs : EventArgs
    {
        public SqlContext Context { get; internal set; }
        public object Result { get; internal set; }

        internal SqlAfterEventArgs() { }
    }

    public class TransactionFinishEventArgs : EventArgs
    {
        public bool IsCommited { get; private set; }

        internal TransactionFinishEventArgs(bool isCommited)
        {
            IsCommited = isCommited;
        }
    }
}
