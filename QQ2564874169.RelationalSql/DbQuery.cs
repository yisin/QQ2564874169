﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace QQ2564874169.RelationalSql
{
    public interface IDbQueryCore : IDisposable
    {
        Hashtable Data { get; }
        bool InTransaction { get; }

        event EventHandler<DbQueryBeforeEventArgs> Before;
        event EventHandler<DbQueryAfterEventArgs> After;
        event EventHandler<DbQueryErrorEventArgs> Error;

        ITransactionScope BeginTransaction();
    }

    public interface IDbQuery : IDbSqlQuery, IDbEntityQuery
    {
        
    }

    public class DbQuery : DbEntityQuery, IDbQuery
    {
        public new static event Action<IDbQuery> Created;

        private IDbSqlQuery _query;

        public DbQuery(IDbSqlQuery query) : base(query)
        {
            _query = query;

            Created?.Invoke(this);
        }

        public T QueryScalar<T>(string sql, object param = null, bool isProc = false, int? timeout = null)
        {
            return _query.QueryScalar<T>(sql, param, isProc, timeout);
        }

        public IEnumerable<T> Query<T>(string sql, object param = null, bool isProc = false, int? timeout = null)
        {
            return _query.Query<T>(sql, param, isProc, timeout);
        }

        public IMultipleReader QueryMultiple(string sql, object param = null, bool isProc = false, int? timeout = null)
        {
            return _query.QueryMultiple(sql, param, isProc, timeout);
        }
    }
}
