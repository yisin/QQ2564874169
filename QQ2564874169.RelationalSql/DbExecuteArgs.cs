﻿using System;

namespace QQ2564874169.RelationalSql
{
    public class DbExecuteBeforeEventArgs : EventArgs
    {
        public DbExecuteOperation Operation { get; internal set; }
        public object Model { get; internal set; }
        public Type TableType { get; internal set; }
        public int? Result { get; set; }

        public ISql Sql { get; }

        internal DbExecuteBeforeEventArgs(ISql sql)
        {
            Sql = sql;
        }

    }

    public class DbExecuteAfterEventArgs : EventArgs
    {
        public DbExecuteOperation Operation { get; }
        public object Model { get; }
        public Type TableType { get; }
        public int Result { get; }
        public ISql Sql { get; }

        internal DbExecuteAfterEventArgs(DbExecuteBeforeEventArgs before)
        {
            Operation = before.Operation;
            Model = before.Model;
            TableType = before.TableType;
            Result = before.Result.GetValueOrDefault();
            Sql = before.Sql;
        }
    }

    public class DbExecuteErrorEventArgs : EventArgs
    {
        public DbExecuteBeforeEventArgs BeforeArgs { get; private set; }
        public Exception Error { get; private set; }
        public int? Result { get; set; }

        internal DbExecuteErrorEventArgs(DbExecuteBeforeEventArgs before, Exception ex)
        {
            Error = ex;
            BeforeArgs = before;
        }
    }

    public class DbUpdateEventArgs : EventArgs
    {
        public object Where { get; private set; }
        public object Sets { get; private set; }
        public bool IsSetNull { get; private set; }

        internal DbUpdateEventArgs(object where, object sets, bool isSetNull)
        {
            Where = where;
            Sets = sets;
            IsSetNull = isSetNull;
        }
    }

    public class DbExecuteException : Exception
    {
        
    }
}
