﻿using System.Collections.Generic;

namespace QQ2564874169.RelationalSql
{
    public class PageResult<T> : List<T>
    {
        public PageSort PageSet { get; }
        public int DataSum { get; }
        public int PageSum { get; }

        public PageResult(int index, int count, int sum, IEnumerable<T> data)
            : this(new PageSort(index, count), sum, data)
        {
        }

        public PageResult(PageSort pageset, int sum, IEnumerable<T> data)
        {
            AddRange(data);
            PageSet = pageset;
            DataSum = sum;

            if (DataSum < pageset.PageCount)
            {
                PageSum = 1;
            }
            else
            {
                PageSum = DataSum / pageset.PageCount;
                if (PageSum * pageset.PageCount < DataSum)
                    PageSum++;
            }
        }
    }
}
