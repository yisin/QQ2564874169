﻿using System.Collections.Generic;

namespace QQ2564874169.RelationalSql
{
    public class ExecuteParam
    {
        public IDictionary<string, object> Parameters { get; }
        public string Command { get; set; }
        public bool IsProc { get; set; }

        public ExecuteParam()
        {
            Parameters = new Dictionary<string, object>();
        }
    }
}
