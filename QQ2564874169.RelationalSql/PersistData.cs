﻿using System;

namespace QQ2564874169.RelationalSql
{
    public class PersistData
    {
        public Guid Id { get; set; }
        public DbExecuteOperation Operation { get; set; }
        public object Model { get; set; }
        public Type TableType { get; set; }
        public Type ServiceType { get; set; }
        public long CreateTime { get; set; }
    }
}
