﻿using System;

namespace QQ2564874169.RelationalSql
{
    public interface IWatchInsertAfter<in T>
    {
        void OnInsertAfter(T model);
    }

    public interface IWatchDeleteAfter<in T>
    {
        void OnDeleteAfter(T model);
    }

    public interface IWatchUpdateAfter<in T>
    {
        void OnUpdateAfter(T setModel, T whereModel);
    }

    public interface IWatchSetNullAfter<in T>
    {
        void OnSetNullAfter(T setModel, T whereModel);
    }

    public interface IWatchProcAfter<in T>
    {
        void OnProcAfter(T model);
    }

    public interface IWatchAfter
    {
        void OnAfter(DbExecuteOperation operation, Type tableType, object model);
    }

    public interface IWatchInsertBefore<in T>
    {
        void OnInsertBefore(T model);
    }

    public interface IWatchDeleteBefore<in T>
    {
        void OnDeleteBefore(T model);
    }

    public interface IWatchUpdateBefore<in T>
    {
        void OnUpdateBefore(T setModel, T whereModel);
    }

    public interface IWatchSetNullBefore<in T>
    {
        void OnSetNullBefore(T setModel, T whereModel);
    }

    public interface IWatchProcBefore<in T>
    {
        void OnProcBefore(T model);
    }

    public interface IWatchBefore
    {
        void OnBefore(DbExecuteOperation operation, Type tableType, object model);
    }

    public class DbExecuteMonitorEventArgs : EventArgs
    {
        public DbExecuteOperation Operation { get; internal set; }
        public object Model { get; internal set; }
        public object Service { get; internal set; }
        public Type TableType { get; internal set; }
        public bool IsBeforeHandler { get; internal set; }

        internal DbExecuteMonitorEventArgs()
        {
        }
    }

    public class DbExecuteMonitorErrorEventArgs : EventArgs
    {
        public DbExecuteOperation Operation { get; internal set; }
        public object Model { get; internal set; }
        public Type TableType { get; internal set; }
        public Type ServiceType { get; internal set; }
        public Exception Error { get; internal set; }
    }

    //public class DbExecuteMonitorOption
    //{
    //    public int ParalleMax { get; set; }
    //    public Func<IDbExecuteMonitorResolver> CreateResolver { get; set; }

    //    public DbExecuteMonitorOption(Func<IDbExecuteMonitorResolver> func)
    //    {
    //        ParalleMax = 10;
    //        CreateResolver = func;
    //    }
    //}
}
