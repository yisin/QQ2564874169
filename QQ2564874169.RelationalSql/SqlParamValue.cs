﻿
namespace QQ2564874169.RelationalSql
{
    public class SqlParamValue
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public object Type { get; set; }
        public int? Size { get; set; }
    }
}
