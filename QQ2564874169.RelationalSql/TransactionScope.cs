﻿using System;

namespace QQ2564874169.RelationalSql
{
    public interface ITransactionScope : IDisposable
    {
        event EventHandler<TransactionScopeFinishEventArgs> Finish;

        void Commit();
    }

    public class TransactionScopeFinishEventArgs : EventArgs
    {
        public bool IsCommited { get; private set; }

        internal TransactionScopeFinishEventArgs(bool isCommited)
        {
            IsCommited = isCommited;
        }
    }

    internal class TransactionScope : ITransactionScope
    {
        public event EventHandler<TransactionScopeFinishEventArgs> Finish;
        private bool _exit;

        public void Dispose()
        {
            if (_exit == false)
                OnFinish(false);
            Finish = null;
            _exit = true;
        }

        public void Commit()
        {
            if(_exit)
                throw new ObjectDisposedException(nameof(TransactionScope));
            OnFinish(true);
            _exit = true;
        }

        private void OnFinish(bool isCommited)
        {
            Finish?.Invoke(this, new TransactionScopeFinishEventArgs(isCommited));
        }
    }
}
