﻿using System;

namespace QQ2564874169.RelationalSql
{
    public class DbQueryBeforeEventArgs : EventArgs
    {
        public string Command { get; internal set; }
        public object Param { get; internal set; }
        public bool IsProc { get; internal set; }
        public int? Timeout { get; internal set; }
        public object Result { get; set; }

        internal DbQueryBeforeEventArgs()
        {
            
        }
    }

    public class DbQueryAfterEventArgs : EventArgs
    {
        public string Command { get; internal set; }
        public object Param { get; internal set; }
        public bool IsProc { get; internal set; }
        public int? Timeout { get; internal set; }
        public object Result { get; internal set; }

        internal DbQueryAfterEventArgs(DbQueryBeforeEventArgs before)
        {
            Command = before.Command;
            Param = before.Param;
            IsProc = before.IsProc;
            Timeout = before.Timeout;
            Result = before.Result;
        }
    }

    public class DbQueryErrorEventArgs : DbQueryBeforeEventArgs
    {
        public Exception Error { get; internal set; }

        internal DbQueryErrorEventArgs(DbQueryBeforeEventArgs before, Exception ex)
        {
            Command = before.Command;
            Param = before.Param;
            IsProc = before.IsProc;
            Timeout = before.Timeout;
            Error = ex;
        }
    }
}
