﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace QQ2564874169.RelationalSql
{
    internal static class Exts
    {
        public static void Add<TK, TV>(this IDictionary<TK, TV> source, IDictionary<TK, TV> args)
        {
            foreach (var k in args.Keys)
            {
                source.Add(k, args[k]);
            }
        }

        public static object GetDefaultValue(this Type t)
        {
            return t.IsValueType ? Activator.CreateInstance(t) : null;
        }

        public static object GetValue(this PropertyInfo property, object instance)
        {
            return property.GetValue(instance, null);
        }

        public static void SetValue(this PropertyInfo property, object instance, object value)
        {
            property.SetValue(instance, value, null);
        }
    }
}
