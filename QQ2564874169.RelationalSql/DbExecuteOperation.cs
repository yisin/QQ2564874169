﻿
namespace QQ2564874169.RelationalSql
{
    public enum DbExecuteOperation
    {
        Insert = 1,
        Delete = 2,
        Update = 3,
        SetNull = 4,
        Proc = 5
    }
}
