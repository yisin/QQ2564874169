﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;

namespace QQ2564874169.RelationalSql
{
    internal static class InternalUtils
    {
        internal static IEnumerable<T> ToArray<T>(this IDataReader reader)
        {
            var list = new List<T>();
            var mt = typeof(T);
            if (mt.IsGenericType && mt.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                mt = mt.GetGenericArguments().First();
            }

            while (reader.Read())
            {
                var model = Activator.CreateInstance<T>();
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    var v = reader[i];
                    if (v == null || v is DBNull)
                        continue;
                    
                    if (mt.IsPrimitive || mt == typeof(string) || mt == typeof(DateTime))
                    {
                        model = (T) CheckType(v, mt);
                        break;
                    }
                    var pi = mt.GetProperty(reader.GetName(i),
                            BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance |
                            BindingFlags.IgnoreCase);
                    if (pi != null)
                    {
                        pi.SetValue(model, CheckType(v, pi.PropertyType));
                    }
                }
                list.Add(model);
            }
            return list.ToArray();
        }

        private static object CheckType(object value, Type conversionType)
        {
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (value == null)
                    return null;
                conversionType = new NullableConverter(conversionType).UnderlyingType;
            }
            return Convert.ChangeType(value, conversionType);
        }
    }
}
