﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    public class Member
    {
        public long? Id { get; set; }
        public string RegName { get; set; }
        public DateTime? CreateTime { get; set; }
    }
}
