﻿using System;
using Autofac;
using QQ2564874169.RelationalSql.Trigger;

namespace TestConsole
{
    public class TriggerResolver : ITriggerResolver
    {
        private ILifetimeScope _scope;

        public TriggerResolver(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public void Dispose()
        {
            _scope?.Dispose();
            _scope = null;
        }

        public ITriggerResolver NewScope()
        {
            return new TriggerResolver(_scope.BeginLifetimeScope());
        }

        public object Resolve(Type type)
        {
            return _scope.Resolve(type);
        }
    }
}
