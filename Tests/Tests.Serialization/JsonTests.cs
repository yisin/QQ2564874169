﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QQ2564874169.Serialization;
using QQ2564874169.Serialization.JsonConverter;
using QQ2564874169.Serialization.JsonResolver;

namespace Tests.Serialization
{
    [TestClass]
    public class JsonTests
    {
        [TestMethod]
        public void 可以直接处理时间类型()
        {
            var now = DateTime.Now;
            var date = Json.ToJson(now);
            var sdate = Json.JsonTo<DateTime>(date);
            Assert.AreEqual(now.Ticks, sdate.Ticks);
        }

        [TestMethod]
        public void 序列化后字段名开头小写()
        {
            var obj = new {A = 1, Bb = 2, cc = 3, dD = 4};
            var obj2 = new {a = 1, bb = 2, cc = 3, dD = 4};
            var j_1 = Json.ToJson(obj);
            var j_2 = Json.ToJson(obj2);
            Assert.AreEqual(j_2, j_1);
        }

        [TestMethod]
        public void 动态类型()
        {
            var c = DateTime.Now;
            var json = Json.ToJson(new {a = 1, b = new[] {"1", "2"}, c, d = new {d_1 = 15.3}});
            var obj = Json.JsonTo(json);
            Assert.AreEqual(1, (int)obj.a);
            Assert.AreEqual("2", (string)obj.b[1]);
            Assert.AreEqual(c, (DateTime)obj.c);
            Assert.AreEqual(15.3, (double) obj.d.d_1);
        }

        [TestMethod]
        public void 超过指定位数的long类型在序列化时改成string类型()
        {
            LongToStringConverter.ConvertByLength = 5;

            long l = 123456;
            var json = Json.ToJson(l);
            var r_l = Json.JsonTo<long>(json);
            Assert.AreEqual(l, r_l);
            Assert.AreEqual($"\"{l}\"", json);

            l = 123;
            json = Json.ToJson(l);
            r_l = Json.JsonTo<long>(json);
            Assert.AreEqual(l, r_l);
            Assert.AreEqual(l.ToString(), json);
        }
    }
}
