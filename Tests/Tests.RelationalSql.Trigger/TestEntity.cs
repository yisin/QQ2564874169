﻿using System;

namespace Tests.RelationalSql.Trigger
{
    public class TestEntity
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? Result { get; set; }
    }

    public class TestEntity2: TestEntity
    {

    }

    public class TestEntity3 : TestEntity
    {

    }
}
