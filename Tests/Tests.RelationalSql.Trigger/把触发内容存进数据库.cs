﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QQ2564874169.RelationalSql;
using QQ2564874169.RelationalSql.Trigger;
using QQ2564874169.TypeContainer.Autofac;
using Tests.RelationalSql.Trigger.Mocks;

namespace Tests.RelationalSql.Trigger
{
    [TestClass]
    public class 把触发内容存进数据库
    {
        [TestMethod]
        public void 增删改都会触发新增一个TriggerItem()
        {
            var cb = new ContainerBuilder();
            cb.RegisterType<MockSql>().AsImplementedInterfaces().InstancePerLifetimeScope();
            cb.RegisterType<MockDbExecute>().AsImplementedInterfaces();
            cb.RegisterType<TriggerPublisher>().AsSelf();
            var container = new AutofacContainer(cb.Build().BeginLifetimeScope());

            TriggerItem.CreateSchema(new MockSchemaCreater());
            var count = 0;
            var monitor = new DbExecuteMonitor(container);
            monitor.LoadService(typeof(TriggerPublisher));
            monitor.Start();

            DbExecute.Created += execute =>
            {
                execute.Before += (s, e) =>
                {
                    if (e.TableType != typeof(TriggerItem))
                        return;
                    Assert.IsTrue(((DbExecute)s).InTransaction);
                    var item = (TriggerItem)e.Model;
                    if (count == 0)
                    {
                        if (item.Type != TriggerType.Insert)
                            return;
                    }
                    else if (count == 1)
                    {
                        if (item.Type != TriggerType.Update)
                            return;
                    }
                    else if (count == 2)
                    {
                        if (item.Type != TriggerType.Delete)
                            return;
                    }
                    count++;
                };
            };

            using (var sql = new MockSql())
            {
                using (var execute = new MockDbExecute(sql))
                {
                    Assert.IsFalse(execute.InTransaction);
                    execute.Insert(new TestEntity { Id = 1, Name = "2", CreateTime = DateTime.Now, Result = 1 });
                    Assert.IsFalse(execute.InTransaction);
                    Assert.AreEqual(1, count);

                    Assert.IsFalse(execute.InTransaction);
                    execute.Update(new TestEntity2 { Name = "5", Id = 1, Result = 1 });
                    Assert.IsFalse(execute.InTransaction);
                    Assert.AreEqual(2, count);

                    Assert.IsFalse(execute.InTransaction);
                    execute.Delete(new TestEntity3 { Name = "10", Id = 1, Result = 1 });
                    Assert.IsFalse(execute.InTransaction);
                    Assert.AreEqual(3, count);
                }
            }

            monitor.Close();
            Assert.AreEqual(3, count);
        }
    }
}
