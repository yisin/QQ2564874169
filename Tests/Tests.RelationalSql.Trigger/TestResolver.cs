﻿using System;
using QQ2564874169.RelationalSql;

namespace Tests.RelationalSql.Trigger
{
    public class TestResolver : IDbExecuteMonitorResolver
    {
        public static event Func<Type, ISql, object> OnCreateBeforeInstance;

        public void Dispose()
        {
        }

        public IDbExecuteMonitorResolver NewScope()
        {
            return new TestResolver();
        }

        public object CreateBeforeInstance(Type serviceType, ISql sql)
        {
            var obj = OnCreateBeforeInstance?.Invoke(serviceType, sql);
            return obj ?? Activator.CreateInstance(serviceType);
        }

        public object CreateAfterInstance(Type serviceType)
        {
            return Activator.CreateInstance(serviceType);
        }
    }
}
