﻿using System;
using System.Collections.Generic;
using QQ2564874169.RelationalSql;

namespace Tests.RelationalSql.Trigger.Mocks
{
    public class MockSql : Sql
    {
        protected override void OnDispose()
        {

        }

        protected override int OnExecute(SqlContext context)
        {
            return int.Parse(context.Command);
        }

        protected override T OnQueryScalar<T>(SqlContext context)
        {
            return (T)context.Param;
        }

        protected override IEnumerable<T> OnQuery<T>(SqlContext context)
        {
            return (IEnumerable<T>)context.Param;
        }

        protected override IMultipleReader OnQueryMultiple(SqlContext context)
        {
            return new MockMultipleReader(context);
        }

        protected override void OnBeginTransaction()
        {

        }

        protected override void OnCommitTransaction()
        {

        }

        protected override void OnRollbackTransaction()
        {

        }
    }

    public class MockMultipleReader : IMultipleReader
    {
        private SqlContext _context;
        private int _index;
        private object[] Result
        {
            get { return (object[]) _context.Param; }
        }


        public MockMultipleReader(SqlContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            Disposed?.Invoke(this, new EventArgs());
        }

        public IEnumerable<T> Read<T>()
        {
            return (IEnumerable<T>) Result[_index++];
        }

        public event EventHandler Disposed;
    }
}
