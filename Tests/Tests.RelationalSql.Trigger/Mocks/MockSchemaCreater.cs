﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using QQ2564874169.RelationalSql.Trigger;

namespace Tests.RelationalSql.Trigger.Mocks
{
    public class MockSchemaCreater: ISchemaCreater
    {
        public void Dispose()
        {
            
        }

        public void CreateTable(Type tableModel)
        {
        }

        public void CreateIndex(PropertyInfo[] columns)
        {
        }
    }
}
