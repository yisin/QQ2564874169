﻿using System;
using QQ2564874169.Core;

namespace Tests.RelationalSql.Trigger.Mocks
{
    public class TriggerContainer : ITypeContainer
    {
        public event Func<Type, object> OnResolve;
         
        public void Dispose()
        {
            OnResolve = null;
        }

        public ITypeContainer NewContainer(params ServiceParam[] param)
        {
            var container = new TriggerContainer();
            container.OnResolve += OnResolve;
            return container;
        }

        public object Resolve(Type type)
        {
            return OnResolve?.Invoke(type);
        }
    }
}
