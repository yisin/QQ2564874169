﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QQ2564874169.RelationalSql.Trigger;

namespace Tests.RelationalSql.Trigger.Mocks
{
    public class MockTriggerResolver: ITriggerResolver
    {
        public static event Func<Type, object> OnResolve;
         
        public void Dispose()
        {

        }

        public ITriggerResolver NewScope()
        {
            return new MockTriggerResolver();
        }

        public object Resolve(Type type)
        {
            return OnResolve?.Invoke(type);
        }
    }
}
