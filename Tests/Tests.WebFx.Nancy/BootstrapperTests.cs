﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nancy.Testing;
using QQ2564874169.WebFx.Nancy;

namespace Tests.WebFx.Nancy
{
    [TestClass]
    public class BootstrapperTests
    {
        [TestMethod]
        public void Test()
        {
            var bootstrapper = new NancyBootstrapper();
            var browser = new Browser(bootstrapper);
            browser.Get("/");
            browser.Get("/");
            browser.Get("/");
            Assert.AreEqual(1, 1);
        }
    }
}
