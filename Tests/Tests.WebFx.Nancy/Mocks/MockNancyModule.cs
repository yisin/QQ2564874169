﻿using Nancy;

namespace Tests.WebFx.Nancy.Mocks
{
    public class MockNancyModule : NancyModule
    {
        public MockNancyModule()
        {
            Get["/"] = _ => "MockNancyModule";
        }
    }
}
