﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QQ2564874169.Nubbo;

namespace Tests.Nubbo.Mocks
{
    class MockInvoker : IServiceInvoker
    {
        private MockServer _server;
        private ResponseResult _result;

        public MockInvoker(MockServer server)
        {
            _server = server;
        }

        public void Dispose()
        {

        }

        public Task<ResponseResult> Invoke(NubboNode node, string serviceName, RequestParam param)
        {
            _result = null;
            _server.OnNewRequest(this, serviceName, param);
            return Task.Run(() =>
            {
                while (_result == null)
                {
                    Task.Delay(1).Wait();
                }
                return _result;
            });
        }

        public void SetResult(ResponseResult result)
        {
            _result = result;
        }
    }
}
