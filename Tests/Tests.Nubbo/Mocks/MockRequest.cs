﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QQ2564874169.Nubbo;

namespace Tests.Nubbo.Mocks
{
    class MockRequest : NubboRequest
    {
        private string _name;

        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreateTime { get; set; }

        public MockRequest(string uri) : base(uri)
        {
            _name = uri;
        }

        protected override RequestParam ConvertToRequestParam()
        {
            return new RequestParam
            {
                Param =
                {
                    {nameof(Id), Id.ToString()},
                    {nameof(Name), Name},
                    {nameof(CreateTime), CreateTime.ToString()}
                }
            };
        }

        protected override NubboNode GetNode()
        {
            return null;
        }

        protected override string GetServiceName()
        {
            return _name;
        }
    }
}
