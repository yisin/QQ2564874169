﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QQ2564874169.Nubbo;

namespace Tests.Nubbo.Mocks
{
    class MockResolver : IResolver
    {
        public event Func<Type, object> OnResolve;
         
        public void Dispose()
        {

        }

        public T Resolve<T>()
        {
            return (T) Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            return OnResolve?.Invoke(type);
        }
    }
}
