﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QQ2564874169.Lock;

namespace Tests.Lock
{
    [TestClass]
    public class ParallelLockspaceTests
    {
        [TestMethod]
        public void 设置的最大并行数有效果()
        {
            var obj = new object();
            var count = 0;
            var max = 10;
            var runningMax = 0;

            Parallel.For(0, 100, i =>
            {
                lock (obj)
                {
                    count++;
                }
                runningMax = runningMax < count ? count : runningMax;
                Task.Delay(10).Wait();
                lock (obj)
                {
                    count--;
                }
            });

            Assert.IsTrue(runningMax > max);
            runningMax = 0;
            count = 0;

            using (var space = new ParallelLockspace(max))
            {
                Parallel.For(0, 100, i =>
                {
                    using (space.Lock())
                    {
                        lock (obj)
                        {
                            count++;
                        }
                        runningMax = runningMax < count ? count : runningMax;
                        Task.Delay(10).Wait();
                        lock (obj)
                        {
                            count--;
                        }
                    }
                });
            }
            Assert.IsTrue(runningMax <= max);
        }
    }
}
