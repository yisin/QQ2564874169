﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QQ2564874169.MessageQueue;

namespace Tests.MessageQueue.Mocks
{
    public class MockResolver : IResolver
    {
        public static event Func<Type, object> OnResolve;
         
        public void Dispose()
        {

        }

        public object Resolve(Type serviceType)
        {
            return OnResolve != null ? OnResolve(serviceType) : Activator.CreateInstance(serviceType);
        }
    }
}
