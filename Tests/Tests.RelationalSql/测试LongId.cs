﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QQ2564874169.RelationalSql;

namespace Tests.RelationalSql
{
    [TestClass]
    public class 测试LongId
    {
        [TestMethod]
        public void 高并发下不会出现相同的Id()
        {
            LongId.Seqlength = 5;
            var map = new ConcurrentDictionary<long, bool>();
            Parallel.For(0, 1000, i1 =>
            {
                Parallel.For(0, 1000, i2 =>
                {
                    var id = LongId.New();
                    if (map.TryAdd(id, true) == false)
                    {
                        Assert.IsTrue(false);
                    }
                });
            });
        }
    }
}
