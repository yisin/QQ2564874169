﻿using System;
using System.Reflection;
using QQ2564874169.RelationalSql;

namespace Tests.RelationalSql.Mocks
{
    public class MockDbExecute : DbExecute
    {
        public MockDbExecute(ISql sql) : base(sql)
        {
        }

        protected override ExecuteParam OnCreateSql(DbExecuteOperation operation, Type tableType, object model)
        {
            int result;
            if (model is DbUpdateEventArgs)
            {
                model = ((DbUpdateEventArgs) model).Sets;
            }
            if (int.TryParse(model.ToString(), out result) == false)
            {
                var p = model.GetType().GetProperty("result", BindingFlags.IgnoreCase);
                if (p == null)
                {
                    result = -1;
                }
                else
                {
                    var v = p.GetValue(model);
                    if (int.TryParse(v.ToString(), out result) == false)
                    {
                        result = -1;
                    }
                }
            }
            return new ExecuteParam {Command = result.ToString()};
        }
    }
}
