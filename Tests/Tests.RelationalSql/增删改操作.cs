﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tests.RelationalSql.Mocks;

namespace Tests.RelationalSql
{
    [TestClass]
    public class 增删改操作
    {
        [TestMethod]
        public void 如果在Before事件中设置了Result就会直接返回结果()
        {
            const int result = 5;
            using (var sql = new MockSql())
            {
                using (var execute = new MockDbExecute(sql))
                {
                    var r = execute.Insert(2);
                    Assert.AreEqual(2, r);

                    execute.Before += (s, e) =>
                    {
                        e.Result = result;
                    };
                    r = execute.Insert(1);
                    Assert.AreEqual(result, r);
                }
            }
        }
    }
}
