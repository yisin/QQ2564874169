﻿using System;
using QQ2564874169.RelationalSql;

namespace Tests.RelationalSql
{
    public class TestResolver : IDbExecuteMonitorResolver
    {
        public event Action<Type, ISql> OnCreateBeforeInstance;

        public void Dispose()
        {
        }

        public IDbExecuteMonitorResolver NewScope()
        {
            return new TestResolver();
        }

        public object CreateBeforeInstance(Type serviceType, ISql sql)
        {
            OnCreateBeforeInstance?.Invoke(serviceType, sql);
            return Activator.CreateInstance(serviceType);
        }

        public object CreateAfterInstance(Type serviceType)
        {
            return Activator.CreateInstance(serviceType);
        }
    }
}
