﻿using System;
using QQ2564874169.Core;

namespace Tests.RelationalSql
{
    public class MonitorContainer : ITypeContainer
    {
        public event Func<Type, object> OnResolve;
        public event Action<ServiceParam[]> OnNewContainer;

        public void Dispose()
        {
            OnResolve = null;
        }

        public ITypeContainer NewContainer(params ServiceParam[] param)
        {
            OnNewContainer?.Invoke(param);
            var container = new MonitorContainer();
            container.OnResolve += OnResolve;
            return container;
        }

        public object Resolve(Type serviceType)
        {
            var obj = OnResolve?.Invoke(serviceType);
            return obj ?? Activator.CreateInstance(serviceType);
        }
    }
}
