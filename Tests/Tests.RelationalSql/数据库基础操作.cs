﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QQ2564874169.RelationalSql;
using Tests.RelationalSql.Mocks;

namespace Tests.RelationalSql
{
    [TestClass]
    public class 数据库基础操作
    {
        [TestMethod]
        public void 事务嵌套时提交次数需要和开启次数一致才会真正提交()
        {
            var count = 0;
            using (var sql = new MockSql())
            {
                sql.TransactionAfter += (s, e) =>
                {
                    if (e.IsCommited)
                        count++;
                };
                using (var t = sql.BeginTransaction())
                {
                    using (var t2 = sql.BeginTransaction())
                    {
                        using (var t3 = sql.BeginTransaction())
                        {
                            t3.Commit();
                        }
                        t2.Commit();
                    }
                    t.Commit();
                }
            }
            Assert.AreEqual(1, count);
        }

        private static void CheckParam(object param, IDictionary<string, object> spvs)
        {
            var count = 0;
            using (var sql = new MockSql())
            {
                sql.Before += (s, e) =>
                {
                    foreach (var item in (ICollection<SqlParamValue>)e.Context.Param)
                    {
                        if (spvs.ContainsKey(item.Name))
                        {
                            if (spvs[item.Name].Equals(item.Value))
                                count++;
                        }
                    }
                };
                sql.Execute("1", param);
            }
            Assert.AreEqual(spvs.Count, count);
        }

        [TestMethod]
        public void Object参数()
        {
            var date = DateTime.Now;
            CheckParam(
                new { a = 1, b = "2", date },
                new Dictionary<string, object>
                {
                    {"a", 1},
                    {"b", "2"},
                    {"date", date}
                });
        }

        [TestMethod]
        public void Collection参数()
        {
            var date = DateTime.Now;
            CheckParam(
                new List<SqlParameter>
                {
                    new SqlParameter("a", 1),
                    new SqlParameter("b", "2"),
                    new SqlParameter("date", date)
                },
                new Dictionary<string, object>
                {
                    {"a", 1},
                    {"b", "2"},
                    {"date", date}
                });
        }

        [TestMethod]
        public void Dictionary参数()
        {
            var date = DateTime.Now;
            CheckParam(
                new Hashtable
                {
                    {"a", 1},
                    {"b", "2"},
                    {"date", date}
                },
                new Dictionary<string, object>
                {
                    {"a", 1},
                    {"b", "2"},
                    {"date", date}
                });
        }
    }
}
