﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Nancy;

namespace QQ2564874169.WebFx.Nancy.Web
{
    [Flags]
    public enum RequestMethod
    {
        Get = 1,
        Post = 2,
        Delete = 4,
        Put = 8
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class RouteSetting : Attribute
    {
        public IList<string> Routes { get; private set; }
        public RequestMethod Method { get; internal set; }
        internal MethodInfo Action { get; set; }

        public RouteSetting(RequestMethod type, string[] routes)
        {
            Method = type;
            Routes = (routes ?? new string[0]).Where(i => i != null).ToList();
        }

        public virtual MethodInfo GetAction(NancyContext context)
        {
            return Action;
        }
    }

    public class GetAttribute : RouteSetting
    {
        public GetAttribute(params string[] routes)
            : base(RequestMethod.Get, routes)
        {

        }
    }

    public class PostAttribute : RouteSetting
    {
        public PostAttribute(params string[] routes)
            : base(RequestMethod.Post, routes)
        {

        }
    }

    public class GetAndPost : RouteSetting
    {
        public GetAndPost(params string[] routes)
            : base(RequestMethod.Get | RequestMethod.Post, routes)
        {
        }
    }
}
