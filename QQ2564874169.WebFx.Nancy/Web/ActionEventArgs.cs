﻿using System;
using System.Reflection;

namespace QQ2564874169.WebFx.Nancy.Web
{
    public class ActionBeforeEventArgs : EventArgs
    {
        public MethodInfo Action { get; internal set; }
        public object Response { get; set; }
    }

    public class ActionAfterEventArgs : EventArgs
    {
        public MethodInfo Action { get; internal set; }
    }

    public class ActionErrorEventArgs : EventArgs
    {
        public MemberInfo Action { get; internal set; }
        public Exception Ex { get; internal set; }
        public object Response { get; set; }
    }
}
