﻿using System;

namespace QQ2564874169.WebFx.Nancy.Web
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ControllerNameAttribute : Attribute
    {
        public string Name { get; set; }

        public ControllerNameAttribute(string name)
        {
            Name = name;
        }
    }
}
