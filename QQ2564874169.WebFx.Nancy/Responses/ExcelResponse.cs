﻿using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Helpers;

namespace QQ2564874169.WebFx.Nancy.Responses
{
    public class ExcelResponse : Response
    {
        public string FileName { get; private set; }
        public DataTable Data { get; private set; }
        public Encoding ContentEncoding { get; set; }
        public string CsvText { get; private set; }
        private byte[] _data;

        public ExcelResponse(string fileName, DataTable data = null)
        {
            FileName = fileName;
            Data = data;
            Contents += WirteContent;
            CsvText = "";
            ContentEncoding = Encoding.UTF8;

            DataTableToCsvText();
        }

        private void DataTableToCsvText()
        {
            if (Data == null) return;

            var content = new StringBuilder();

            foreach (DataColumn column in Data.Columns)
            {
                content.Append(column.ColumnName).Append("\t");
            }
            if (content.Length > 0)
            {
                content.Append("\n");
            }
            foreach (DataRow row in Data.Rows)
            {
                for (var i = 0; i < Data.Columns.Count; i++)
                {
                    content.Append(row[i]).Append("\t");
                }
                content.Append("\n");
            }
            if (content.Length < 1) return;

            CsvText = content.ToString();
        }

        private void WirteContent(Stream stream)
        {
            if (_data != null)
            {
                stream.Write(_data, 0, _data.Length);
            }
        }

        public override Task PreExecute(NancyContext context)
        {
            var task = base.PreExecute(context);
            _data = ContentEncoding.GetBytes(CsvText);
            var fileName = HttpUtility.UrlEncode(FileName);
            StatusCode = HttpStatusCode.OK;
            ContentType = "application/ms-excel";
            this.WithHeader("Content-Disposition", string.Format("attachment; filename={0}.xls", fileName));
            this.WithHeader("Content-Length", _data.Length.ToString());
            return task;
        }
    }
}
