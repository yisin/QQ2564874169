﻿using Owin;

namespace QQ2564874169.WebFx.Nancy
{
    public class NancyStartup
    {
        public void Configuration(IAppBuilder builder)
        {
            OnConfiguration(builder);
        }

        protected virtual void OnConfiguration(IAppBuilder builder)
        {
            builder.UseNancy();
        }
    }
}
