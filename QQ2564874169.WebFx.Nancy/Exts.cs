﻿using System;
using System.Linq;

namespace QQ2564874169.WebFx.Nancy
{
    internal static class Exts
    {
        public static string ToHex(this string str)
        {
            var data = Convert.FromBase64String(str);
            return string.Join("", data.Select(i => i.ToString("X2")));
        }
    }
}
