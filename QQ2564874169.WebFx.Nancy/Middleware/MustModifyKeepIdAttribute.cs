﻿using System;

namespace QQ2564874169.WebFx.Nancy.Middleware
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class MustModifyKeepIdAttribute : Attribute
    {

    }
}
