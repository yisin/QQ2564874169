﻿using System;

namespace QQ2564874169.WebFx.Nancy.Middleware
{
    public class ClientKeepOption
    {
        /// <summary>
        /// 多久没有请求自动退出登录（分钟），默认20分钟。
        /// </summary>
        public int LogoutWhenIdle { get; set; }

        /// <summary>
        /// 秘钥
        /// </summary>
        public string Secret { get; set; }

        /// <summary>
        /// 用户保持状态的参数名。
        /// </summary>
        public string KeepName { get; set; }

        /// <summary>
        /// 是否在ajax请求下更新keepId。
        /// </summary>
        public bool KeepIdUpdateWhenAjax { get; set; }

        public bool SaveToCookie { get; set; }

        public bool SaveToHeader { get; set; }

        /// <summary>
        /// 获取ClientUser。
        /// </summary>
        public Func<ClientParam, ClientUser> GetClientUser { get; set; }

        public ClientKeepOption()
        {
            LogoutWhenIdle = 20;
            Secret = "QQ2564874169";
            KeepName = "_auth_";
            SaveToCookie = true;
        }
    }
}
