﻿using System;
using QQ2564874169.Core.Encryption;
using QQ2564874169.Core.Utils;

namespace QQ2564874169.WebFx.Nancy.Middleware
{
    public class ClientParam
    {
        public DateTime Expire { get; internal set; }
        public bool IsAbsTime { get; internal set; }
        public string UserId { get; internal set; }
        public bool IsLogin { get; internal set; }
        public object State { get; internal set; }

        internal string GetSign(string secret)
        {
            var ltime = ConvertHelper.ToLongTime(Expire);
            var signstr = string.Join(".", UserId, IsAbsTime, ltime, secret);
            return Encrypt.MD5(signstr, len16: true).ToHex();
        }
    }
}
