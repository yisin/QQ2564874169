﻿using System;
using System.Collections.Generic;
using Nancy.Security;
using QQ2564874169.Core.Encryption;
using QQ2564874169.Core.Utils;

namespace QQ2564874169.WebFx.Nancy.Middleware
{
    public class ClientUser : IUserIdentity
    {
        public string Id { get; private set; }
        public string UserName { get; }
        public IEnumerable<string> Claims { get; }
        public UserData Data { get; private set; }
        internal bool IsLogin { get; set; }
        internal bool IsLogout { get; set; }
        internal ClientParam Param { get; set; }

        public ClientUser(string id, string userName = null)
        {
            Id = id;
            UserName = userName;
            Claims = new string[0];
            Data = new UserData();
        }

        public class UserData : Dictionary<string, object>
        {
            internal UserData()
                : base(StringComparer.OrdinalIgnoreCase)
            {

            }

            public new void Add(string key, object value)
            {
                this[key] = value;
            }

            public new object this[string key]
            {
                get { return ContainsKey(key) ? base[key] : null; }
                set
                {
                    if (value == null)
                    {
                        if (ContainsKey(key))
                            Remove(key);
                        return;
                    }

                    if (ContainsKey(key))
                    {
                        base[key] = value;
                    }
                    else
                    {
                        base.Add(key, value);
                    }
                }
            }
        }
    }
}
