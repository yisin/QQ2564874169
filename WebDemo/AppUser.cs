﻿using QQ2564874169.WebFx.Nancy.Middleware;

namespace WebDemo
{
    public class AppUser : ClientUser
    {
        public AppUser(string id, string userName = null) : base(id, userName)
        {
        }
    }
}