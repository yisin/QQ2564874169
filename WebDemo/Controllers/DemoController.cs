﻿using System;
using Nancy;
using QQ2564874169.WebFx.Nancy;
using QQ2564874169.WebFx.Nancy.Web;

namespace WebDemo.Controllers
{
    public class DemoController : AppController
    {
        [Get("/demo", "/demo/index")]
        public object Index()
        {
            return GetViewByModel(DateTime.Now);
        }

        [Get("loginpage")]
        public object Login()
        {
            return GetView();
        }

        [Post]
        public object TryLogin()
        {
            if (CurrentUser != null)
                throw new Exception("请先退出登录。");
            
            var name = this.Params("name");

            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("请输入用户名。");

            this.ClientLogin(name);
            return Response.AsRedirect("/demo/member");
        }

        [Get]
        [SetPermission(AppPermission.已登录)]
        public object Member()
        {
            var name = CurrentUser.Id;
            return GetViewByModel(new {name});
        }

        [Get("/demo/logout")]
        public object Logout()
        {
            this.ClientLogout();
            return Response.AsRedirect("/demo/loginpage");
        }
    }
}