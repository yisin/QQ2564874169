﻿using Nancy.ViewEngines.Razor;

namespace WebDemo
{
    public abstract class AppView : AppView<dynamic>
    {
        
    }

    public abstract class AppView<T> : NancyRazorViewBase<T>
    {

    }
}