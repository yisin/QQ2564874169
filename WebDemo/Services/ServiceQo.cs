﻿using Autofac;
using QQ2564874169.RelationalSql;

namespace WebDemo.Services
{
    public abstract class ServiceQo<T> : DomainObject<T>
    {
        protected IDbQuery DbQuery { get; private set; }

        protected abstract T OnQuery(ServiceContext context);

        protected internal sealed override T OnUse(DomainContext context, object state = null)
        {
            var ctx = (ServiceContext) state;
            DbQuery = ctx.Container.Resolve<IDbQuery>();
            return OnQuery(ctx);
        }
    }
}