﻿using System;

namespace WebDemo.Services
{
    public class DomainObjectEventArgs : EventArgs
    {
        public object DomainObject { get; internal set; }
        public DomainContext Context { get; internal set; }
        public object Result { get; set; }
    }

    public class DomainObjectErrorEventArgs : EventArgs
    {
        public object DomainObject { get; internal set; }
        public DomainContext Context { get; internal set; }
        public Exception Error { get; internal set; }
    }
}