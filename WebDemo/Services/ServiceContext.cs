﻿using System;
using Autofac;

namespace WebDemo.Services
{
    public class ServiceContext
    {
        public static event Action<ServiceContext> Created;
        public ILifetimeScope Container { get; }
        public DomainContext Context { get; }

        public ServiceContext(ILifetimeScope container, DomainContext context)
        {
            Container = container;
            Context = context;
            Created?.Invoke(this);
        }

        public T UseBo<T>(T bo) where T : ServiceBo
        {
            Context.Use(bo, this);
            return bo;
        }

        public T UseVo<T>(ServiceQo<T> vo)
        {
            return Context.Use(vo, this);
        }
    }
}