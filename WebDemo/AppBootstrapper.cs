﻿using System;
using Autofac;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses;
using QQ2564874169.WebFx.Nancy;

namespace WebDemo
{
    public class AppBootstrapper : NancyBootstrapper
    {
        protected override ILifetimeScope GetApplicationContainer()
        {
            return AppStartup.Container;
        }

        protected override void ApplicationStartup(ILifetimeScope container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);

            pipelines.UseClientKeeper();
            pipelines.OnError.AddItemToStartOfPipeline(OnError);
        }

        private static Response OnError(NancyContext context, Exception error)
        {
            if (error is NoLoginException)
            {
                return new RedirectResponse("/demo/loginPage");
            }
            return null;
        }
    }
}