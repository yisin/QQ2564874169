﻿using System;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;

namespace WebDemo
{
    public class AppLogs
    {
        public static string Dir { get; set; }
        
        public static ILogger GetLogger(string name)
        {
            return LogManager.GetLogger(name);
        }

        static AppLogs()
        {
            if (string.IsNullOrEmpty(Dir))
                throw new NullReferenceException(nameof(Dir));

            var configuration = new LoggingConfiguration();
            var dir = Path.Combine(Dir, "${logger}", "${level}", "${shortdate}.log");
            var file = new FileTarget("file")
            {
                FileName = Layout.FromString(dir),
                Layout = Layout.FromString("${longdate}${newline}${message}${newline")
            };
            configuration.AddTarget(file);
            configuration.AddRuleForAllLevels(file);
            LogManager.Configuration = configuration;
        }
    }
}