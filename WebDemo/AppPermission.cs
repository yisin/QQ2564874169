﻿using System;

namespace WebDemo
{
    public enum AppPermission
    {
        无限制,
        已登录
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class SetPermission : Attribute
    {
        public AppPermission Permission { get; }

        public SetPermission(AppPermission permission = AppPermission.无限制)
        {
            Permission = permission;
        }
    }

    public class NoLoginException : Exception
    {
        public string RequestUrl { get; }

        public NoLoginException(string url)
        {
            RequestUrl = url;
        }
    }
}