﻿using QQ2564874169.WebFx.Nancy.Web;

namespace WebDemo.Apis
{
    public class DemoApi : AppApi
    {
        [Get("/apidemo")]
        public object Index()
        {
            return ExecutingRoute.GetAction(Context).Name;
        }

        [Get("/apidemo")]
        public object Index_5()
        {
            return ExecutingRoute.GetAction(Context).Name;
        }

        [Get("/apidemo")]
        public object Index_10()
        {
            return ExecutingRoute.GetAction(Context).Name;
        }
    }
}