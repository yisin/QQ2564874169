﻿using System.Reflection;
using QQ2564874169.WebFx.Nancy.Web;

namespace WebDemo
{
    public abstract class AppController : NancyController
    {
        protected AppUser CurrentUser => (AppUser) Context.CurrentUser;

        protected AppController()
        {
            ActionBefore += VerifyLoginStatus;
        }

        private void VerifyLoginStatus(object sender, ActionBeforeEventArgs e)
        {
            var attr = e.Action.GetCustomAttribute<SetPermission>();
            attr = attr ?? e.Action.DeclaringType.GetCustomAttribute<SetPermission>();
            if (attr == null || attr.Permission == AppPermission.无限制)
                return;
            if (CurrentUser == null)
                throw new NoLoginException(Request.Url.ToString());
        }
    }
}