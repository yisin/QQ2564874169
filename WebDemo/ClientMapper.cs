﻿using System;
using QQ2564874169.WebFx.Nancy.Middleware;

namespace WebDemo
{
    public interface IClientMapper : IDisposable
    {
        ClientUser Get(string userId);
    }

    public class ClientMapper : IClientMapper
    {
        public ClientUser Get(string userId)
        {
            return new AppUser(userId, userId);
        }

        public void Dispose()
        {

        }
    }
}