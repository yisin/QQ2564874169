﻿$(function () {
    $("input[type=button]")
        .click(function () {
            var frm = $(this).closest("form");
            var data = [];
            $(frm)
                .find("input:not(input[type=button])")
                .each(function (i, n) {
                    data.push({ name: $(n).attr("name"), value: $(n).val() });
                });
            var m = frm.attr("method").toLowerCase();
            var url = frm.attr("action") + "?" + parseInt(Math.random() * 1000000);
            if (m === "get") {
                $.each(data,
                    function (i, n) {
                        url += "&" + n.name + "=" + encodeURIComponent(n.value);
                    });
                $.get(url,
                    function (resdata) {
                        frm.append($("<div></div>").text(JSON.stringify(resdata)));
                    });
            } else if (m === "post") {
                var pm = {};
                $.each(data,
                    function (i, n) {
                        pm[n.name] = encodeURIComponent(n.value);
                    });
                $.post(url,
                    pm,
                    function (resdata) {
                        frm.append($("<div></div>").text(JSON.stringify(resdata)));
                    });
            }
        });
});