﻿
using System;
using System.Collections.Generic;

namespace QQ2564874169.Nubbo
{
    public class ServiceResult
    {
        public IDictionary<string,string> Header { get; }
        public object Data { get; set; }
        public string Error { get; set; }

        internal ServiceResult()
        {
            Header = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }
    }

    public class ServiceContext
    {
        public ServiceNameAttribute ServiceName { get; }
        public ServiceRequest Request { get; internal set; }
        public ServiceResult Result { get; }

        internal ServiceContext(ServiceNameAttribute serviceName)
        {
            ServiceName = serviceName;
            Result = new ServiceResult();
        }

        internal void AddEx(string message)
        {
            if (Result.Error != null)
            {
                Result.Error += Environment.NewLine + message;
            }
            else
            {
                Result.Error = message;
            }
        }
    }
}
