﻿using System;
using System.Collections.Generic;

namespace QQ2564874169.Nubbo
{
    public class RequestParam
    {
        public IDictionary<string, string> Header { get; }
        public IDictionary<string, string> Param { get; }

        public RequestParam()
        {
            Header = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            Param = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }
    }
}
