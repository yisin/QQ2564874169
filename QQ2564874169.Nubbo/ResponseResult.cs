﻿using System;
using System.Collections.Generic;

namespace QQ2564874169.Nubbo
{
    public class ResponseResult
    {
        public IDictionary<string, string> Header { get; }
        public byte[] Data { get; set; }
        public string Error { get; set; }

        public ResponseResult()
        {
            Header = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }
    }
}
