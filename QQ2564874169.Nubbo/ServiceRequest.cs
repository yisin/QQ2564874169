﻿using System;

namespace QQ2564874169.Nubbo
{
    public class ServiceRequest
    {
        public string ServiceName { get; }
        public RequestParam Param { get; }
        private Action<ServiceResult, object> _callback;
        private object _state;

        public ServiceRequest(string serviceName, Action<ServiceResult, object> callback, object state = null)
        {
            Param = new RequestParam();
            ServiceName = serviceName;
            _callback = callback;
            _state = state;
        }

        internal void OnResponse(ServiceResult result)
        {
            _callback(result, _state);
        }
    }
}
