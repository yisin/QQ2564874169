﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace QQ2564874169.Nubbo
{
    public interface IObjectOutput
    {
        byte[] Serialize(object obj);
    }

    public class JsonOutput : IObjectOutput
    {
        public byte[] Serialize(object obj)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                DateFormatString = "yyyy-MM-dd HH:mm:ss.ffffff"
            }));
        }
    }
}
