﻿using System;

namespace QQ2564874169.Nubbo
{
    public class RequestException : Exception
    {
        internal RequestException(string message = null, Exception ex = null) : base(message, ex)
        {

        }
    }

    public class ResponseException : Exception
    {
        internal ResponseException(string message = null, Exception ex = null) : base(message, ex)
        {

        }
    }

    public class RemoteServiceException : Exception
    {
        internal RemoteServiceException(string message = null, Exception ex = null) : base(message, ex)
        {

        }
    }
}
