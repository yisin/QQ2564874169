﻿using System;

namespace QQ2564874169.Nubbo
{
    public interface IResolver : IDisposable
    {
        T Resolve<T>();
        object Resolve(Type type);
    }
}
