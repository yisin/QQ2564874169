﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QQ2564874169.Nubbo
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ServiceNameAttribute : Attribute
    {
        public string Name { get; set; }

        public ServiceNameAttribute(string name)
        {
            Name = name;
        }
    }

    public interface IService
    {
        void Do(ServiceContext context);
    }
}
