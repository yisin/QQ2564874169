﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace QQ2564874169.Nubbo
{
    public interface IObjectInput
    {
        object Deserialize(Type type, byte[] data);
    }

    public class JsonInput : IObjectInput
    {
        public object Deserialize(Type type, byte[] data)
        {
            var json = Encoding.UTF8.GetString(data);
            return JsonConvert.DeserializeObject(json, type);
        }
    }
}
