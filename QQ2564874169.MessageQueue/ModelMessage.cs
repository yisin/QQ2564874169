﻿
namespace QQ2564874169.MessageQueue
{
    internal class ModelMessage
    {
        public string Id { get; set; }
        public string ModelType { get; set; }
        public string Model { get; set; }
        public long Time { get; set; }
        public string SQueue { get; set; }
    }
}
