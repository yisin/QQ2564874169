﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QQ2564874169.MessageQueue
{
    public class MqMessage
    {
        public string Queue { get; private set; }
        public string Content { get; private set; }
        public object State { get; private set; }
        public bool Reconsume { get; set; }

        public MqMessage(string queue, string content, object state = null)
        {
            Content = content;
            Queue = queue;
            State = state;
        }
    }
}
