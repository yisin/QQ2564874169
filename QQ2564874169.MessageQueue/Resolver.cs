﻿using System;

namespace QQ2564874169.MessageQueue
{
    public interface IResolver : IDisposable
    {
        object Resolve(Type serviceType);
    }
}
