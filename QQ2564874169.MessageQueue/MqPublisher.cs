﻿using System;

namespace QQ2564874169.MessageQueue
{
    public interface IMqPublisher: IDisposable
    {
        void Send(string queue, params string[] message);
    }

    public class AppMqPublisher: IMqPublisher
    {
        internal static event Action<string, string[]> OnSend; 

        public void Dispose()
        {

        }

        public void Send(string queue, params string[] message)
        {
            OnSend?.Invoke(queue, message);
        }
    }
}
