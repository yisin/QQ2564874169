﻿using System;

namespace QQ2564874169.MessageQueue
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public abstract class ToMq : Attribute
    {
        public virtual string SubscribeQueue => GetType().Name;
        public virtual string PublishQueue => GetType().Name;

        public abstract IMqPublisher Publisher { get; }
        public abstract IMqSubscriber Subscriber { get; }
    }
}
