﻿using System;
using System.Linq;
using QQ2564874169.Core.Utils;

namespace QQ2564874169.MessageQueue
{
    internal static class Exts
    {
        public static DateTime ToDate(this long time, DateAccuracy accuracy = DateAccuracy.Second)
        {
            return ConvertHelper.ToDate(time, accuracy);
        }

        public static long ToLongTime(this DateTime time, DateAccuracy accuracy = DateAccuracy.Second)
        {
            return ConvertHelper.ToLongTime(time, accuracy);
        }

        public static T[] GetCustomAttributes<T>(this Type t, bool inherit = false)
        {
            return t.GetCustomAttributes(typeof(T), inherit).Cast<T>().ToArray();
        }

        public static T GetCustomAttribute<T>(this Type t, bool inherit = false)
        {
            return t.GetCustomAttributes<T>().FirstOrDefault();
        }
    }
}
