﻿using System.IO;

namespace QQ2564874169.File
{
    public interface IFile
    {
        void Create(string path, Stream file);
        bool ExistsFile(string path);
        void RemoveFile(string path);
        string GetUri(string path);
        Stream GetFile(string path);
    }
}
