﻿using System;
using System.Runtime.InteropServices;

namespace QQ2564874169.Miniblink
{
    internal class MBApi
    {
        private const string DLL = "node.dll";

        [DllImport(DLL, EntryPoint = "wkeIsInitialize", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeIsInitialize();

        [DllImport(DLL, EntryPoint = "wkeInitialize", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeInitialize();

        [DllImport(DLL, EntryPoint = "wkeInitializeEx", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeInitializeEx(WKESettings settings);

        [DllImport(DLL, EntryPoint = "wkeFinalize", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeFinalize();

        [DllImport(DLL, EntryPoint = "wkeConfigure", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeConfigure(WKESettings settings);

        [DllImport(DLL, EntryPoint = "wkeSetDebugConfig", CallingConvention = CallingConvention.Cdecl,
            CharSet = CharSet.Unicode)]
        public static extern void wkeSetDebugConfig(IntPtr webView, string debugString, string param);

        [DllImport(DLL, EntryPoint = "wkeGetVersion", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint wkeGetVersion();

        [DllImport(DLL, EntryPoint = "wkeGetVersionString", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetVersionString();

        [DllImport(DLL, EntryPoint = "wkeCreateWebView", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeCreateWebView();

        [DllImport(DLL, EntryPoint = "wkeGetWebView", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr wkeGetWebView(string name);

        [DllImport(DLL, EntryPoint = "wkeDestroyWebView", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeDestroyWebView(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeSetMemoryCacheEnable", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetMemoryCacheEnable(IntPtr webView, [MarshalAs(UnmanagedType.I1)]bool b);

        [DllImport(DLL, EntryPoint = "wkeSetNavigationToNewWindowEnable", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetNavigationToNewWindowEnable(IntPtr webView, [MarshalAs(UnmanagedType.I1)]bool b);

        [DllImport(DLL, EntryPoint = "wkeSetNpapiPluginsEnabled", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetNpapiPluginsEnabled(IntPtr webView, [MarshalAs(UnmanagedType.I1)]bool b);

        [DllImport(DLL, EntryPoint = "wkeSetHeadlessEnabled", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetHeadlessEnabled(IntPtr webView, [MarshalAs(UnmanagedType.I1)]bool b);

        [DllImport(DLL, EntryPoint = "wkeSetCspCheckEnable", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetCspCheckEnable(IntPtr webView, [MarshalAs(UnmanagedType.I1)]bool b);

        [DllImport(DLL, EntryPoint = "wkeSetProxy", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetProxy(WKEProxy proxy);

        [DllImport(DLL, EntryPoint = "wkeSetViewProxy", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetViewProxy(IntPtr webView, WKEProxy proxy);

        [DllImport(DLL, EntryPoint = "wkeSetHandle", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetHandle(IntPtr webView, IntPtr wndHandle);

        [DllImport(DLL, EntryPoint = "wkeSetHandleOffset", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetHandleOffset(IntPtr webView, int x, int y);

        [DllImport(DLL, EntryPoint = "wkeIsTransparent", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeIsTransparent(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeSetTransparent", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetTransparent(IntPtr webView, [MarshalAs(UnmanagedType.I1)]bool transparent);

        [DllImport(DLL, EntryPoint = "wkeSetUserAgent", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeSetUserAgent(IntPtr webView, string userAgent);

        [DllImport(DLL, EntryPoint = "wkeGetUserAgent", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern IntPtr wkeGetUserAgent(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeLoadURL", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeLoadURL(IntPtr webView, string url);

        [DllImport(DLL, EntryPoint = "wkeLoadHTML", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeLoadHTML(IntPtr webView, string html);

        [DllImport(DLL, EntryPoint = "wkeLoadHtmlWithBaseUrl", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeLoadHtmlWithBaseUrl(IntPtr webView, string html, string baseUrl);

        [DllImport(DLL, EntryPoint = "wkeLoadFileW", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeLoadFileW(IntPtr webView, string fileName);

        [DllImport(DLL, EntryPoint = "wkeGetURL", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetURL(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeIsLoading", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeIsLoading(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeIsDocumentReady", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeIsDocumentReady(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeStopLoading", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeStopLoading(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeReload", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeReload(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGetTitle", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetTitle(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeResize", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeResize(IntPtr webView, int w, int h);

        [DllImport(DLL, EntryPoint = "wkeGetWidth", CallingConvention = CallingConvention.Cdecl)]
        public static extern int wkeGetWidth(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGetHeight", CallingConvention = CallingConvention.Cdecl)]
        public static extern int wkeGetHeight(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGetContentWidth", CallingConvention = CallingConvention.Cdecl)]
        public static extern int wkeGetContentWidth(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGetContentHeight", CallingConvention = CallingConvention.Cdecl)]
        public static extern int wkeGetContentHeight(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkePaint2", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkePaint2(IntPtr webView, IntPtr bits, int bufWid, int bufHei, int xDst, int yDst, int w, int h, int xSrc, int ySrc, [MarshalAs(UnmanagedType.I1)]bool bCopyAlpha);

        [DllImport(DLL, EntryPoint = "wkePaint", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkePaint(IntPtr webView, IntPtr bits, int pitch);

        [DllImport(DLL, EntryPoint = "wkeGetViewDC", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetViewDC(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGetHostHWND", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetHostHWND(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeCanGoBack", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeCanGoBack(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGoBack", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeGoBack(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeCanGoForward", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeCanGoForward(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGoForward", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeGoForward(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeEditorSelectAll", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeEditorSelectAll(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeEditorUnSelect", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeEditorUnSelect(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeEditorCopy", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeEditorCopy(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeEditorCut", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeEditorCut(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeEditorPaste", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeEditorPaste(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeEditorDelete", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeEditorDelete(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeEditorUndo", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeEditorUndo(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeEditorRedo", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeEditorRedo(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGetCookie", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetCookie(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeFreeCookieList", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeFreeCookieList(IntPtr cookieList);

        [DllImport(DLL, EntryPoint = "wkePerformCookieCommand", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkePerformCookieCommand(wkeCookieCommand command);

        [DllImport(DLL, EntryPoint = "wkeSetCookieEnabled", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetCookieEnabled(IntPtr webView, [MarshalAs(UnmanagedType.I1)]bool enable);

        [DllImport(DLL, EntryPoint = "wkeIsCookieEnabled", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeIsCookieEnabled(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeSetCookieJarPath", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeSetCookieJarPath(IntPtr webView, string path);

        [DllImport(DLL, EntryPoint = "wkeSetCookieJarFullPath", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeSetCookieJarFullPath(IntPtr webView, string path);

        [DllImport(DLL, EntryPoint = "wkeSetMediaVolume", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetMediaVolume(IntPtr webView, float volume);

        [DllImport(DLL, EntryPoint = "wkeGetMediaVolume", CallingConvention = CallingConvention.Cdecl)]
        public static extern float wkeGetMediaVolume(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeFireMouseEvent", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeFireMouseEvent(IntPtr webView, int message, int x, int y, int flags);

        [DllImport(DLL, EntryPoint = "wkeFireContextMenuEvent", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeFireContextMenuEvent(IntPtr webView, int x, int y, uint flags);

        [DllImport(DLL, EntryPoint = "wkeFireMouseWheelEvent", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeFireMouseWheelEvent(IntPtr webView, int x, int y, int delta, uint flags);

        [DllImport(DLL, EntryPoint = "wkeFireKeyUpEvent", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeFireKeyUpEvent(IntPtr webView, int virtualKeyCode, uint flags, [MarshalAs(UnmanagedType.I1)]bool systemKey);

        [DllImport(DLL, EntryPoint = "wkeFireKeyDownEvent", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeFireKeyDownEvent(IntPtr webView, int virtualKeyCode, uint flags, [MarshalAs(UnmanagedType.I1)]bool systemKey);

        [DllImport(DLL, EntryPoint = "wkeFireKeyPressEvent", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeFireKeyPressEvent(IntPtr webView, int charCode, uint flags, [MarshalAs(UnmanagedType.I1)]bool systemKey);

        [DllImport(DLL, EntryPoint = "wkeFireWindowsMessage", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeFireWindowsMessage(IntPtr webView, IntPtr hWnd, uint message, IntPtr wParam, IntPtr lParam, IntPtr result);

        [DllImport(DLL, EntryPoint = "wkeSetFocus", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeSetFocus(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeKillFocus", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeKillFocus(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGetCaretRect", CallingConvention = CallingConvention.Cdecl)]
        public static extern wkeRect wkeGetCaretRect(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeRunJSW", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern long wkeRunJSW(IntPtr webView, string script);

        [DllImport(DLL, EntryPoint = "wkeGlobalExec", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGlobalExec(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeSetZoomFactor", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetZoomFactor(IntPtr webView, float factor);

        [DllImport(DLL, EntryPoint = "wkeGetZoomFactor", CallingConvention = CallingConvention.Cdecl)]
        public static extern float wkeGetZoomFactor(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeGetString", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetString(IntPtr wkeString);

        [DllImport(DLL, EntryPoint = "wkeGetStringW", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetStringW(IntPtr wkeString);

        [DllImport(DLL, EntryPoint = "wkeSetStringW", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeSetStringW(IntPtr wkeString, string str, int len);

        [DllImport(DLL, EntryPoint = "wkeCreateStringW", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern IntPtr wkeCreateStringW(string str, int len);

        [DllImport(DLL, EntryPoint = "wkeDeleteString", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeDeleteString(IntPtr wkeString);

        [DllImport(DLL, EntryPoint = "wkeGetWebViewForCurrentContext", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetWebViewForCurrentContext();

        [DllImport(DLL, EntryPoint = "wkeSetUserKeyValue", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void wkeSetUserKeyValue(IntPtr webView, string key, IntPtr value);

        [DllImport(DLL, EntryPoint = "wkeGetUserKeyValue", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr wkeGetUserKeyValue(IntPtr webView, string key);

        [DllImport(DLL, EntryPoint = "wkeGetCursorInfoType", CallingConvention = CallingConvention.Cdecl)]
        public static extern wkeCursorInfo wkeGetCursorInfoType(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeSetDragFiles", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeSetDragFiles(IntPtr webView, IntPtr clintPos, IntPtr screenPos, [MarshalAs(UnmanagedType.LPArray)]IntPtr[] files, int filesCount);

        [DllImport(DLL, EntryPoint = "wkeOnTitleChanged", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnTitleChanged(IntPtr webView, wkeTitleChangedCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnURLChanged", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnURLChanged(IntPtr webView, wkeURLChangedCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnURLChanged2", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnURLChanged2(IntPtr webView, wkeURLChangedCallback2 callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnPaintUpdated", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnPaintUpdated(IntPtr webView, wkePaintUpdatedCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnAlertBox", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnAlertBox(IntPtr webView, wkeAlertBoxCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnConfirmBox", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnConfirmBox(IntPtr webView, wkeConfirmBoxCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnPromptBox", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnPromptBox(IntPtr webView, wkePromptBoxCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnNavigation", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnNavigation(IntPtr webView, wkeNavigationCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnCreateView", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnCreateView(IntPtr webView, wkeCreateViewCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnDocumentReady", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnDocumentReady(IntPtr webView, wkeDocumentReadyCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnDocumentReady2", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnDocumentReady2(IntPtr webView, wkeDocumentReady2Callback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnLoadingFinish", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnLoadingFinish(IntPtr webView, wkeLoadingFinishCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnDownload", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnDownload(IntPtr webView, wkeDownloadCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnConsole", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnConsole(IntPtr webView, wkeConsoleCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeNetOnResponse", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeNetOnResponse(IntPtr webView, wkeNetResponseCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnLoadUrlBegin", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnLoadUrlBegin(IntPtr webView, wkeLoadUrlBeginCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnLoadUrlEnd", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnLoadUrlEnd(IntPtr webView, wkeLoadUrlEndCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnDidCreateScriptContext", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnDidCreateScriptContext(IntPtr webView, wkeDidCreateScriptContextCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeOnWillReleaseScriptContext", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeOnWillReleaseScriptContext(IntPtr webView, wkeWillReleaseScriptContextCallback callback, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeNetSetMIMEType", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void wkeNetSetMIMEType(IntPtr job, string type);

        [DllImport(DLL, EntryPoint = "wkeNetSetHTTPHeaderField", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeNetSetHTTPHeaderField(IntPtr job, string key, string value, [MarshalAs(UnmanagedType.I1)]bool response);

        [DllImport(DLL, EntryPoint = "wkeNetSetData", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void wkeNetSetData(IntPtr job, [MarshalAs(UnmanagedType.LPArray)]byte[] buf, int len);

        // 调用此函数后,网络层收到数据会存储在一buf内,接收数据完成后响应OnLoadUrlEnd事件.#此调用严重影响性能,慎用
        // 此函数和wkeNetSetData的区别是，wkeNetHookRequest会在接受到真正网络数据后再调用回调，并允许回调修改网络数据。
        // 而wkeNetSetData是在网络数据还没发送的时候修改
        [DllImport(DLL, EntryPoint = "wkeNetHookRequest", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeNetHookRequest(IntPtr job);

        [DllImport(DLL, EntryPoint = "wkeNetHoldJobToAsynCommit", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeNetHoldJobToAsynCommit(IntPtr job);

        [DllImport(DLL, EntryPoint = "wkeNetGetPostBody", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeNetGetPostBody(IntPtr job);

        [DllImport(DLL, EntryPoint = "wkeNetChangeRequestUrl", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void wkeNetChangeRequestUrl(IntPtr job, string url);

        [DllImport(DLL, EntryPoint = "wkeNetContinueJob", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeNetContinueJob(IntPtr job);

        [DllImport(DLL, EntryPoint = "wkeNetGetRequestMethod", CallingConvention = CallingConvention.Cdecl)]
        public static extern wkeRequestType wkeNetGetRequestMethod(IntPtr job);

        [DllImport(DLL, EntryPoint = "wkeWebFrameIsMainFrame", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeWebFrameIsMainFrame(IntPtr webFrame);

        [DllImport(DLL, EntryPoint = "wkeIsWebRemoteFrame", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte wkeIsWebRemoteFrame(IntPtr webFrame);

        [DllImport(DLL, EntryPoint = "wkeWebFrameGetMainFrame", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeWebFrameGetMainFrame(IntPtr webView);

        [DllImport(DLL, EntryPoint = "wkeWebFrameGetMainWorldScriptContext", CallingConvention = CallingConvention.Cdecl)]
        public static extern void wkeWebFrameGetMainWorldScriptContext(IntPtr webFrame, ref IntPtr contextOut);

        [DllImport(DLL, EntryPoint = "wkeGetBlinkMainThreadIsolate", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetBlinkMainThreadIsolate();

        [DllImport(DLL, EntryPoint = "wkeGetWindowHandle", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr wkeGetWindowHandle(IntPtr WebView);

        [DllImport(DLL, EntryPoint = "wkeJsBindFunction", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void wkeJsBindFunction(string name, wkeJsNativeFunction fn, IntPtr param, uint argCount);

        [DllImport(DLL, EntryPoint = "wkeJsBindGetter", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void wkeJsBindGetter(string name, wkeJsNativeFunction fn, IntPtr param);

        [DllImport(DLL, EntryPoint = "wkeJsBindSetter", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void wkeJsBindSetter(string name, wkeJsNativeFunction fn, IntPtr param);

        [DllImport(DLL, EntryPoint = "jsArgCount", CallingConvention = CallingConvention.Cdecl)]
        public static extern int jsArgCount(IntPtr es);

        [DllImport(DLL, EntryPoint = "jsArgCount", CallingConvention = CallingConvention.Cdecl)]
        public static extern jsType jsArgType(IntPtr es, int argIdx);

        [DllImport(DLL, EntryPoint = "jsArg", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsArg(IntPtr es, int argIdx);

        [DllImport(DLL, EntryPoint = "jsGetKeys", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr jsGetKeys(IntPtr es, long value);

        [DllImport(DLL, EntryPoint = "jsTypeOf", CallingConvention = CallingConvention.Cdecl)]
        public static extern jsType jsTypeOf(long v);

        [DllImport(DLL, EntryPoint = "jsIsNumber", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsNumber(long v);

        [DllImport(DLL, EntryPoint = "jsIsString", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsString(long v);

        [DllImport(DLL, EntryPoint = "jsIsBoolean", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsBoolean(long v);

        [DllImport(DLL, EntryPoint = "jsIsObject", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsObject(long v);

        [DllImport(DLL, EntryPoint = "jsIsFunction", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsFunction(long v);

        [DllImport(DLL, EntryPoint = "jsIsUndefined", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsUndefined(long v);

        [DllImport(DLL, EntryPoint = "jsIsNull", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsNull(long v);

        [DllImport(DLL, EntryPoint = "jsIsArray", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsArray(long v);

        [DllImport(DLL, EntryPoint = "jsIsTrue", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsTrue(long v);

        [DllImport(DLL, EntryPoint = "jsIsFalse", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsIsFalse(long v);

        [DllImport(DLL, EntryPoint = "jsToInt", CallingConvention = CallingConvention.Cdecl)]
        public static extern int jsToInt(IntPtr es, long v);

        [DllImport(DLL, EntryPoint = "jsToFloat", CallingConvention = CallingConvention.Cdecl)]
        public static extern float jsToFloat(IntPtr es, long v);

        [DllImport(DLL, EntryPoint = "jsToDouble", CallingConvention = CallingConvention.Cdecl)]
        public static extern double jsToDouble(IntPtr es, long v);

        [DllImport(DLL, EntryPoint = "jsToBoolean", CallingConvention = CallingConvention.Cdecl)]
        public static extern byte jsToBoolean(IntPtr es, long v);

        [DllImport(DLL, EntryPoint = "jsToTempStringW", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr jsToTempStringW(IntPtr es, long v);

        [DllImport(DLL, EntryPoint = "jsInt", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsInt(int n);

        [DllImport(DLL, EntryPoint = "jsFloat", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsFloat(float f);

        [DllImport(DLL, EntryPoint = "jsDouble", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsDouble(double d);

        [DllImport(DLL, EntryPoint = "jsBoolean", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsBoolean(bool b);

        [DllImport(DLL, EntryPoint = "jsUndefined", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsUndefined();

        [DllImport(DLL, EntryPoint = "jsNull", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsNull();

        [DllImport(DLL, EntryPoint = "jsTrue", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsTrue();

        [DllImport(DLL, EntryPoint = "jsFalse", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsFalse();

        [DllImport(DLL, EntryPoint = "jsString", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern long jsString(IntPtr es, string str);

        [DllImport(DLL, EntryPoint = "jsEmptyObject", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsEmptyObject(IntPtr es);

        [DllImport(DLL, EntryPoint = "jsEmptyArray", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsEmptyArray(IntPtr es);

        [DllImport(DLL, EntryPoint = "jsObject", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsObject(IntPtr es, IntPtr obj);

        [DllImport(DLL, EntryPoint = "jsFunction", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsFunction(IntPtr es, IntPtr obj);

        [DllImport(DLL, EntryPoint = "jsGetData", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr jsGetData(IntPtr es, long jsValue);

        [DllImport(DLL, EntryPoint = "jsGet", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern long jsGet(IntPtr es, long jsValue, string prop);

        [DllImport(DLL, EntryPoint = "jsSet", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void jsSet(IntPtr es, long jsValue, string prop, long v);

        [DllImport(DLL, EntryPoint = "jsGetAt", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsGetAt(IntPtr es, long jsValue, int index);

        [DllImport(DLL, EntryPoint = "jsSetAt", CallingConvention = CallingConvention.Cdecl)]
        public static extern void jsSetAt(IntPtr es, long jsValue, int index, long v);

        [DllImport(DLL, EntryPoint = "jsGetLength", CallingConvention = CallingConvention.Cdecl)]
        public static extern int jsGetLength(IntPtr es, long jsValue);

        [DllImport(DLL, EntryPoint = "jsSetLength", CallingConvention = CallingConvention.Cdecl)]
        public static extern void jsSetLength(IntPtr es, long jsValue, int length);

        [DllImport(DLL, EntryPoint = "jsGlobalObject", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsGlobalObject(IntPtr es);

        [DllImport(DLL, EntryPoint = "jsGetWebView", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr jsGetWebView(IntPtr es);

        [DllImport(DLL, EntryPoint = "jsEvalW", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern long jsEvalW(IntPtr es, string str);

        [DllImport(DLL, EntryPoint = "jsEvalExW", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern long jsEvalExW(IntPtr es, string str, [MarshalAs(UnmanagedType.I1)]bool isInClosure);

        [DllImport(DLL, EntryPoint = "jsCall", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsCall(IntPtr es, long func, long thisObject, [MarshalAs(UnmanagedType.LPArray)]long[] args, int argCount);

        [DllImport(DLL, EntryPoint = "jsCallGlobal", CallingConvention = CallingConvention.Cdecl)]
        public static extern long jsCallGlobal(IntPtr es, long func, [MarshalAs(UnmanagedType.LPArray)]long[] args, int argCount);

        [DllImport(DLL, EntryPoint = "jsGetGlobal", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern long jsGetGlobal(IntPtr es, string prop);

        [DllImport(DLL, EntryPoint = "jsSetGlobal", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void jsSetGlobal(IntPtr es, string prop, long jsValue);

        [DllImport(DLL, EntryPoint = "jsGC", CallingConvention = CallingConvention.Cdecl)]
        public static extern void jsGC();

        [DllImport(DLL, EntryPoint = "wkeShowDevtools", CallingConvention = CallingConvention.Cdecl,CharSet = CharSet.Unicode)]
        public static extern void wkeShowDevtools(IntPtr webView, string path, wkeOnShowDevtoolsCallback callback,
            IntPtr param);
    }
}
