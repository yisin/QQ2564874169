﻿using System;

namespace QQ2564874169.Miniblink
{
	internal delegate IntPtr WndProcCallback(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

	public delegate IntPtr? WndMsgCallback(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);
}
