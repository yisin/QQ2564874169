﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QQ2564874169.Miniblink
{
	public partial class MiniblinkForm : Form, IMiniblink
	{
		private bool _noneBorderResize;
		/// <summary>
		/// 无边框模式下调整窗体大小
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool NoneBorderResize
		{
			get { return _noneBorderResize; }
			set
			{
				_noneBorderResize = value;

				if (_noneBorderResize)
				{
					_browser.WndMsg += _resizeMsg;
				}
				else
				{
					_browser.WndMsg -= _resizeMsg;
				}
			}
		}

		public FormShadowWidth ShadowWidth { get; }

		public override ContextMenuStrip ContextMenuStrip
		{
			get { return _browser.ContextMenuStrip; }
			set { _browser.ContextMenuStrip = value; }
		}

		private ResizeDirect _direct;
		private bool _resizeing;
		private Point _resize_start;
		private Point _resize_pos;
		private Size _resize_size;
		private bool _isdrop;
		private Point _dropstart;
		private Point _dropWinstart;
		private string _dragfunc;
		private string _maxfunc;
		private string _minfunc;
		private string _closefunc;
		private wkePaintUpdatedCallback _paintUpdated;
		private WndMsgCallback _resizeMsg;
		private WndMsgCallback _shadowMsg;
		private bool m_aeroEnabled;
		private const int CS_DROPSHADOW = 0x00020000;

		public MiniblinkForm(): this(false)
		{

		}

		public MiniblinkForm(bool isTransparent)
		{
			InitializeComponent();

			IsTransparent = isTransparent;
			ShadowWidth = new FormShadowWidth();
			_resizeMsg = new WndMsgCallback(ResizeMsg);

			if (Utils.IsDesignMode()) return;

			if (IsTransparent)
			{
				_paintUpdated = new wkePaintUpdatedCallback(wkePaintUpdated);
				FormBorderStyle = FormBorderStyle.None;
				var style = WinApi.GetWindowLong(Handle, (int)WinConst.GWL_EXSTYLE);
				WinApi.SetWindowLong(Handle, (int)WinConst.GWL_EXSTYLE, style | (int)WinConst.WS_EX_LAYERED);
				MBApi.wkeSetTransparent(_browser.MiniblinkHandle, true);
				MBApi.wkeOnPaintUpdated(_browser.MiniblinkHandle, null, IntPtr.Zero);
				MBApi.wkeOnPaintUpdated(_browser.MiniblinkHandle, _paintUpdated, Handle);
			}

			var tmp = Guid.NewGuid().ToString().Replace("-", "");
			BindNetFunc(new NetFunc(_dragfunc = "drag" + tmp, DropFunc));
			BindNetFunc(new NetFunc(_maxfunc = "max" + tmp, MaxFunc));
			BindNetFunc(new NetFunc(_minfunc = "min" + tmp, MinFunc));
			BindNetFunc(new NetFunc(_closefunc = "close" + tmp, CloseFunc));

			_browser.WndMsg += DropWndMsg;
			NoneBorderResize = true;
			DocumentReady += RegisterJsEvent;
			RegisterNetFunc(this);

			if (CheckAero())
			{
				_shadowMsg = new WndMsgCallback(ShadowWndMsg);
				_browser.WndMsg += _shadowMsg;
			}
		}

		protected override CreateParams CreateParams
		{
			get
			{
				m_aeroEnabled = CheckAero();

				var cp = base.CreateParams;
				if (!m_aeroEnabled)
					cp.ClassStyle |= CS_DROPSHADOW;

				return cp;
			}
		}

		private static bool CheckAero()
		{
			if (Environment.OSVersion.Version.Major >= 6)
			{
				var enabled = 0;
				WinApi.DwmIsCompositionEnabled(ref enabled);
				return enabled == 1;
			}
			return false;
		}

		private IntPtr? ShadowWndMsg(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam)
		{
			if (msg != (int) WinConst.WM_NCPAINT)
				return null;
			if (ShadowWidth.Bottom + ShadowWidth.Left + ShadowWidth.Right + ShadowWidth.Top < 1)
				return null;
			var v = 2;
			WinApi.DwmSetWindowAttribute(Handle, 2, ref v, 4);
			var margins = new MARGINS
			{
				top = ShadowWidth.Top,
				left = ShadowWidth.Left,
				right = ShadowWidth.Right,
				bottom = ShadowWidth.Bottom
			};
			WinApi.DwmExtendFrameIntoClientArea(Handle, ref margins);
			return null;
		}

		private IntPtr? DropWndMsg(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam)
		{
			if (_isdrop == false)
			{
				return null;
			}

			var wMsg = (WinConst) msg;

			switch (wMsg)
			{
				case WinConst.WM_LBUTTONUP:
					_isdrop = false;
					Cursor = Cursors.Default;
					break;
				case WinConst.WM_MOUSEMOVE:
					var x = Utils.LOWORD(lParam);
					var y = Utils.HIWORD(lParam);
					var sp = PointToScreen(new Point(x, y));
					var nx = sp.X - _dropstart.X;
					var ny = sp.Y - _dropstart.Y;
					nx = _dropWinstart.X + nx;
					ny = _dropWinstart.Y + ny;
					Location = new Point(nx, ny);
					Cursor = Cursors.SizeAll;
					return IntPtr.Zero;
			}
			return null;
		}

		private void ResizeTask()
		{
			var last = _resize_start;
			var waiter = new SpinWait();

			Task.Factory.StartNew(() =>
			{
				while (_resizeing)
				{
					if (MouseButtons != MouseButtons.Left)
						break;
					var curr = MousePosition;
					if (curr.X != last.X || curr.Y != last.Y)
					{
						var xx = curr.X - _resize_start.X;
						var xy = curr.Y - _resize_start.Y;
						int nx = Left, ny = Top, nw = Width, nh = Height;

						switch (_direct)
						{
							case ResizeDirect.Left:
								nw = _resize_size.Width - xx;
								nx = _resize_pos.X + xx;
								break;
							case ResizeDirect.Right:
								nw = _resize_size.Width + xx;
								break;
							case ResizeDirect.Top:
								nh = _resize_size.Height - xy;
								ny = _resize_pos.Y + xy;
								break;
							case ResizeDirect.Bottom:
								nh = _resize_size.Height + xy;
								break;
							case ResizeDirect.LeftTop:
								nw = _resize_size.Width - xx;
								nx = _resize_pos.X + xx;
								nh = _resize_size.Height - xy;
								ny = _resize_pos.Y + xy;
								break;
							case ResizeDirect.LeftBottom:
								nw = _resize_size.Width - xx;
								nx = _resize_pos.X + xx;
								nh = _resize_size.Height + xy;
								break;
							case ResizeDirect.RightTop:
								nw = _resize_size.Width + xx;
								nh = _resize_size.Height - xy;
								ny = _resize_pos.Y + xy;
								break;
							case ResizeDirect.RightBottom:
								nw = _resize_size.Width + xx;
								nh = _resize_size.Height + xy;
								break;
						}
						this.SafeInvoke(() =>
						{
							Size = new Size(nw, nh);
							Location = new Point(nx, ny);
						});
					}
					last = curr;
					waiter.SpinOnce();
				}
				_browser.WndMsg -= StopMoveEvent;
			});
		}

		private IntPtr? StopMoveEvent(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam)
		{
			if (WinConst.WM_MOUSEMOVE == (WinConst) msg)
			{
				return IntPtr.Zero;
			}

			return null;
		}

		private IntPtr? ResizeMsg(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam)
		{
			if (FormBorderStyle != FormBorderStyle.None)
			{
				return null;
			}

			var wMsg = (WinConst)msg;

			switch (wMsg)
			{
				case WinConst.WM_LBUTTONDOWN:
					if (_resizeing == false && _direct != ResizeDirect.None)
					{
						_resizeing = true;
						_resize_start = MousePosition;
						_resize_pos = Location;
						_resize_size = Size;
						_browser.WndMsg += StopMoveEvent;
						ResizeTask();
						return IntPtr.Zero;
					}

					return null;
				case WinConst.WM_LBUTTONUP:
					if (_resizeing)
					{
						_resizeing = false;
						_browser.WndMsg -= StopMoveEvent;
					}

					return null;
				case WinConst.WM_MOUSEMOVE:
					if(_resizeing == false)
					{
						const int p = 5;
						var x = Utils.LOWORD(lParam);
						var y = Utils.HIWORD(lParam);
						var rect = ClientRectangle;
						var direct = ResizeDirect.None;

						if (x <= p && x > 0)
						{
							if (y <= p && y > 0)
							{
								direct = ResizeDirect.LeftTop;
							}
							else if (y >= rect.Height - p && y < rect.Height)
							{
								direct = ResizeDirect.LeftBottom;
							}
							else
							{
								direct = ResizeDirect.Left;
							}
						}
						else if (y <= p && y > 0)
						{
							if (x <= p && x > 0)
							{
								direct = ResizeDirect.LeftTop;
							}
							else if (x >= rect.Width - p && x < rect.Width)
							{
								direct = ResizeDirect.RightTop;
							}
							else
							{
								direct = ResizeDirect.Top;
							}
						}
						else if (x >= rect.Width - p && x < rect.Width)
						{
							if (y <= p && y > 0)
							{
								direct = ResizeDirect.RightTop;
							}
							else if (y >= rect.Height - p && y < rect.Height)
							{
								direct = ResizeDirect.RightBottom;
							}
							else
							{
								direct = ResizeDirect.Right;
							}
						}
						else if (y >= rect.Height - p && y < rect.Height)
						{
							if (x <= p && x > 0)
							{
								direct = ResizeDirect.LeftBottom;
							}
							else if (x >= rect.Width - p && x < rect.Width)
							{
								direct = ResizeDirect.RightBottom;
							}
							else
							{
								direct = ResizeDirect.Bottom;
							}
						}
						else if (_direct != ResizeDirect.None)
						{
							Cursor = Cursors.Default;
							_direct = ResizeDirect.None;
							return null;
						}

						_direct = direct;
					}

					switch (_direct)
					{
						case ResizeDirect.Bottom:
						case ResizeDirect.Top:
							Cursor = Cursors.SizeNS;
							break;
						case ResizeDirect.Left:
						case ResizeDirect.Right:
							Cursor = Cursors.SizeWE;
							break;
						case ResizeDirect.LeftTop:
						case ResizeDirect.RightBottom:
							Cursor = Cursors.SizeNWSE;
							break;
						case ResizeDirect.RightTop:
						case ResizeDirect.LeftBottom:
							Cursor = Cursors.SizeNESW;
							break;
					}

					if (_resizeing)
					{
						return IntPtr.Zero;
					}
					return null;
				default:
					return null;
			}
		}

		private void wkePaintUpdated(IntPtr mb, IntPtr param, IntPtr hdc, int x, int y, int w, int h)
		{
			if (IsDisposed) return;
			var point = new WinPoint();
			var bmp = new WinBitmap();
			var hBmp = WinApi.GetCurrentObject(hdc, (int)WinConst.OBJ_BITMAP);
			WinApi.GetObject(hBmp, Marshal.SizeOf(typeof(WinBitmap)), ref bmp);
			var size = new WinSize(bmp.bmWidth, bmp.bmHeight);
			var screen = WinApi.GetDC(Handle);
			var blend = new BlendFunction
			{
				BlendOp = (byte)WinConst.AC_SRC_OVER,
				SourceConstantAlpha = 255,
				AlphaFormat = (byte)WinConst.AC_SRC_ALPHA
			};
			var ok = WinApi.UpdateLayeredWindow(Handle, screen, IntPtr.Zero, ref size, hdc, ref point, 0, ref blend, (int)WinConst.ULW_ALPHA);
			if (ok == 0)
			{
				var hdcMemory = WinApi.CreateCompatibleDC(screen);
				var hbmpMemory = WinApi.CreateCompatibleBitmap(screen, size.width, size.height);
				var hbmpOld = WinApi.SelectObject(hdcMemory, hbmpMemory);

				WinApi.BitBlt(hdcMemory, 0, 0, size.width, size.height, hdc, 0, 0, (int)WinConst.SRCCOPY | (int)WinConst.CAPTUREBLT);
				WinApi.BitBlt(hdc, 0, 0, size.width, size.height, hdcMemory, 0, 0, (int)WinConst.SRCCOPY | (int)WinConst.CAPTUREBLT);
				WinApi.UpdateLayeredWindow(Handle, screen, IntPtr.Zero, ref size, hdcMemory, ref point, 0, ref blend, (int)WinConst.ULW_ALPHA);
				WinApi.SelectObject(hdcMemory, hbmpOld);
				WinApi.DeleteObject(hbmpMemory);
				WinApi.DeleteDC(hdcMemory);
			}

			WinApi.ReleaseDC(Handle, screen);
		}

		private object DropFunc(NetFuncContext context)
		{
			if (_isdrop == false && WindowState != FormWindowState.Maximized && MouseButtons == MouseButtons.Left)
			{
				_isdrop = true;
				_dropstart = MousePosition;
				_dropWinstart = Location;
				Cursor = Cursors.SizeAll;
			}
			return null;
		}

		private object MaxFunc(NetFuncContext context)
		{
			WindowState = WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
			return null;
		}

		private object MinFunc(NetFuncContext context)
		{
			WindowState = FormWindowState.Minimized;
			return null;
		}

		private object CloseFunc(NetFuncContext context)
		{
			Close();
			return null;
		}

		private void RegisterJsEvent(object sender, DocumentReadyEventArgs e)
		{
			if (e.IsMain == false) return;

			_browser.RunJs(@"
                document.getElementsByTagName('body')[0].addEventListener('mousedown',
                    function(e) {
                        var obj = e.target || e.srcElement;
                        if ({ 'INPUT': 1, 'SELECT': 1,'IMG': 1 }[obj.tagName.toUpperCase()])
                        return;

                        while (obj) {
                            for (var i = 0; i<obj.classList.length; i++) {
                                if (obj.classList[i] === 'mbform-nodrag')
                                    return;
                                if (obj.classList[i] === 'mbform-drag') {
                                    " + _dragfunc + @"(e.screenX, e.screenY);
                                    return;
                                }
                            }
                            obj = obj.parentElement;
                        }
                    });


                var els = document.getElementsByClassName('mbform-max');
                for (var i=0;i<els.length;i++)
                {
                    els[i].addEventListener('click',
                        function() {" + _maxfunc + @"(); });
                }

                els = document.getElementsByClassName('mbform-min');
                for (var i=0;i<els.length;i++)
                {
                    els[i].addEventListener('click',
                        function() {" + _minfunc + @"(); });
                }

                els = document.getElementsByClassName('mbform-close');
                for (var i=0;i<els.length;i++)
                {
                    els[i].addEventListener('click',
                        function() {" + _closefunc + @"(); });
                }
            ");
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsTransparent { get; set; }

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public float Zoom
		{
			get { return _browser.Zoom; }
			set { _browser.Zoom = value; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool CookieEnabled
		{
			get { return _browser.CookieEnabled; }
			set { _browser.CookieEnabled = value; }
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string UserAgent
		{
			get { return _browser.UserAgent; }
			set { _browser.UserAgent = value; }
		}

		private FormWindowState _windowState = FormWindowState.Normal;
		private Rectangle? _stateRect;
		public new FormWindowState WindowState
		{
			get { return _windowState; }
			set
			{
				if (FormBorderStyle != FormBorderStyle.None)
				{
					base.WindowState = value;
					return;
				}
				if (_stateRect.HasValue == false)
				{
					_stateRect = new Rectangle(Location, Size);
				}
				var rect = _stateRect.Value;

				if (value == FormWindowState.Maximized)
				{
					if (_windowState != FormWindowState.Maximized)
					{
						_stateRect = new Rectangle(Location, Size);
						Location = new Point(0, 0);
						Size = Screen.PrimaryScreen.WorkingArea.Size;
						base.WindowState = FormWindowState.Normal;
					}
				}
				else if (value == FormWindowState.Normal)
				{
					Location = rect.Location;
					Size = rect.Size;
					base.WindowState = value;
				}
				else
				{
					base.WindowState = value;
				}
				_windowState = value;
			}
		}

		public string LocalDomain => _browser.LocalDomain;
		public string LocalResourceDir => _browser.LocalResourceDir;
		public string Url => _browser.Url;
		public bool IsDocumentReady => _browser.IsDocumentReady;
		public string DocumentTitle => _browser.DocumentTitle;
		public int DocumentWidth => _browser.DocumentWidth;
		public int DocumentHeight => _browser.DocumentHeight;
		public int ContentWidth => _browser.ContentWidth;
		public int ContentHeight => _browser.ContentHeight;
		public bool CanGoBack => _browser.CanGoBack;
		public bool CanGoForward => _browser.CanGoForward;

		public event EventHandler<UrlChangedEventArgs> UrlChanged
		{
			add { _browser.UrlChanged += value; }
			remove { _browser.UrlChanged -= value; }
		}
		public event EventHandler<NavigateEventArgs> NavigateBefore
		{
			add { _browser.NavigateBefore += value; }
			remove { _browser.NavigateBefore -= value; }
		}
		public event EventHandler<DocumentReadyEventArgs> DocumentReady
		{
			add { _browser.DocumentReady += value; }
			remove { _browser.DocumentReady -= value; }
		}
		public event EventHandler<ConsoleMessageEventArgs> ConsoleMessage
		{
			add { _browser.ConsoleMessage += value; }
			remove { _browser.ConsoleMessage -= value; }
		}
		public event EventHandler<NetResponseEventArgs> NetResponse
		{
			add { _browser.NetResponse += value; }
			remove { _browser.NetResponse -= value; }
		}
		public event EventHandler<LoadUrlBeginEventArgs> LoadUrlBegin
		{
			add { _browser.LoadUrlBegin += value; }
			remove { _browser.LoadUrlBegin -= value; }
		}

		public void RegisterNetFunc(object target)
		{
			_browser.RegisterNetFunc(target);
		}

		public void ShowDevTools()
		{
			_browser.ShowDevTools();
		}

		public object RunJs(string script)
		{
			return _browser.RunJs(script);
		}

		public object CallJsFunc(string funcName, params object[] param)
		{
			return _browser.CallJsFunc(funcName, param);
		}

		public void BindNetFunc(NetFunc func)
		{
			_browser.BindNetFunc(func);
		}

		public void SetLocalResource(string dir, string domain)
		{
			_browser.SetLocalResource(dir, domain);
		}

		public void SetHeadlessEnabled(bool enable)
		{
			_browser.SetHeadlessEnabled(enable);
		}

		public void SetNpapiPluginsEnable(bool enable)
		{
			_browser.SetNpapiPluginsEnable(enable);
		}

		public void SetNavigationToNewWindow(bool enable)
		{
			_browser.SetNavigationToNewWindow(enable);
		}

		public void SetCspCheckEnable(bool enable)
		{
			_browser.SetCspCheckEnable(enable);
		}

		public bool GoForward()
		{
			return _browser.GoForward();
		}

		public void EditorSelectAll()
		{
			_browser.EditorSelectAll();
		}

		public void EditorUnSelect()
		{
			_browser.EditorUnSelect();
		}

		public void EditorCopy()
		{
			_browser.EditorCopy();
		}

		public void EditorCut()
		{
			_browser.EditorCut();
		}

		public void EditorPaste()
		{
			_browser.EditorPaste();
		}

		public void EditorDelete()
		{
			_browser.EditorDelete();
		}

		public void EditorUndo()
		{
			_browser.EditorUndo();
		}

		public void EditorRedo()
		{
			_browser.EditorRedo();
		}

		public bool GoBack()
		{
			return _browser.GoBack();
		}

		public void SetProxy(WKEProxy proxy)
		{
			_browser.SetProxy(proxy);
		}

		public void LoadUri(string uri)
		{
			_browser.LoadUri(uri);
		}

		public void LoadHtml(string html, string baseUrl = null)
		{
			_browser.LoadHtml(html, baseUrl);
		}

		public void StopLoading()
		{
			_browser.StopLoading();
		}

		public void Reload()
		{
			_browser.Reload();
		}

		private enum ResizeDirect
		{
			None,
			Left,
			Right,
			Top,
			Bottom,
			LeftTop,
			LeftBottom,
			RightTop,
			RightBottom
		}
	}
}

