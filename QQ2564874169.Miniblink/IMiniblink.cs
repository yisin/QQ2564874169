﻿using System;

namespace QQ2564874169.Miniblink
{
	public interface IMiniblink
	{
		string LocalDomain { get; }
		string LocalResourceDir { get; }
		string Url { get; }
		bool IsDocumentReady { get; }
		string DocumentTitle { get; }
		int DocumentWidth { get; }
		int DocumentHeight { get; }
		int ContentWidth { get; }
		int ContentHeight { get; }
		bool CanGoBack { get; }
		bool CanGoForward { get; }
		float Zoom { get; set; }
		bool CookieEnabled { get; set; }
		string UserAgent { get; set; }
		
		event EventHandler<UrlChangedEventArgs> UrlChanged;
		event EventHandler<NavigateEventArgs> NavigateBefore;
		event EventHandler<DocumentReadyEventArgs> DocumentReady;
		event EventHandler<ConsoleMessageEventArgs> ConsoleMessage;
		event EventHandler<NetResponseEventArgs> NetResponse;
		event EventHandler<LoadUrlBeginEventArgs> LoadUrlBegin;

		void RegisterNetFunc(object target);
		void ShowDevTools();
		object RunJs(string script);
		object CallJsFunc(string funcName, params object[] param);
		void BindNetFunc(NetFunc func);
		void SetLocalResource(string dir, string domain);
		void SetHeadlessEnabled(bool enable);
		void SetNpapiPluginsEnable(bool enable);
		void SetNavigationToNewWindow(bool enable);
		void SetCspCheckEnable(bool enable);
		bool GoForward();
		void EditorSelectAll();
		void EditorUnSelect();
		void EditorCopy();
		void EditorCut();
		void EditorPaste();
		void EditorDelete();
		void EditorUndo();
		void EditorRedo();
		bool GoBack();
		void SetProxy(WKEProxy proxy);
		void LoadUri(string uri);
		void LoadHtml(string html, string baseUrl = null);
		void StopLoading();
		void Reload();
	}
}
