﻿using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace QQ2564874169.Json.JsonResolver
{
    public class JsonContractResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var p = base.CreateProperty(member, memberSerialization);
            p.NullValueHandling = NullValueHandling.Ignore;
            return p;
        }

        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName[0].ToString().ToLower() + propertyName.Substring(1);
        }
    }
}
